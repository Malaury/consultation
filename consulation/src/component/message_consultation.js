import React, { useState} from 'react';
import { Alert } from 'reactstrap';
import { Link } from 'react-router-dom';

const Message_EmptyUserID = () => {
    return (
        <Alert color="warning" className="alert alert-dismissible alert-danger">
            If you want to add a consultation you should start by the beginnig if you are new.
            <Link to="/consultation"><button type="button" className="btn btn-primary">Add new consultation</button></Link>
        </Alert>
    );
}

const Message_NeedToBeConnected = () => {
    return (
        <Alert color="danger" className="alert alert-dismissible alert-danger">
            You have to be connected to send a consultation.
         <Link to="/Login"><button type="button" className="btn btn-primary">Login</button></Link>
        </Alert>
    );
}

const Message_UserIdentified = (props) => {
    let user = props.user;
    return (
        <Alert color="primary" className="alert alert-dismissible alert-info">
            <h4>Welcome {!user ? "" : user.firstname} {!user ? "" : user.lastname}</h4>
            If you are not {!user  ? "" : user.firstname} click <a onClick={() => {localStorage.removeItem('idUser'); props.callBack(true);}}>here.</a>
        </Alert>
    );
}

export { Message_EmptyUserID, Message_NeedToBeConnected, Message_UserIdentified };