const express = require('express');
const router = express.Router();
const {validate} = require('../models/response');
const jwtUtils = require('../utils/jwt.utils');
const db = require('../utils/mysqldb');
const mysql = require('mysql');

const log = require('loglevel');
log.enableAll();

router.get('/text/:responsetext', (req, res) => {
    log.info("api/response - get('/text/:responsetext') | Enter in function.");
    if(jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const queryString = `SELECT * FROM response WHERE text=?`
    db.connection.query(queryString, [req.params.responsetext], (err, rows) => {
        if(err) throw err;
        res.json(rows);
        log.info("api/response - get('/text/:responsetext') | Get response data by text ("+ req.params.responsetext + ") : "+ rows+".");
    });
    log.info("api/response - get('/text/:responsetext') | Function finished.");
});

router.get('/:id', (req, res) => {
    log.info("api/response - get('/:id') | Enter in function.");
    if(jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const queryString = `SELECT * FROM response WHERE idresponse=?`
    db.connection.query(queryString,[req.params.id], (err, rows) => {
        if(err) throw err;
        res.json(rows);
        log.info("api/response - get('/:id') | Get response data by id ("+ req.params.id + ") : "+ rows+".");
    });
    log.info("api/response - get('/:id') | Function finished.");
});

router.post('/', (req,res) => {
    log.info("api/response - put('/') | Enter in function.");
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message)
    if(jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const response = ({
        text: req.body.text
    });
    const queryString = `INSERT INTO response (\`text\`) VALUES (?);`
    db.connection.query(queryString, [response.text], (err, rows) => {
        if(err) throw err;
        log.info("api/response - put('/') | Put response data ("+ response.text + ") : "+ rows+".");
    });
    res.send(response);
    log.info("api/response - put('/') | Function finished.");
});

module.exports = router;