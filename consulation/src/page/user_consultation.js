import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
const urls = require('../utils/frontUtils');

const UserConsultation = (props) => {
    const [questionResponses, setQuestionResponses] = useState([]);
    useEffect(() => {
        axios.get(`${urls.URL_BACKEND}/user_has_question_response/${props.match.params.id}/consultation`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => setQuestionResponses(res.data))
            .catch(error => console.log(error))
    }, [props]);

    let cpt = 1;
    const displayQuestionResponses = questionResponses.map((questionResponse) =>
        <li key={questionResponse.question_text} className="list-group-item">

            <div className="container">
                <div className="row">{cpt++}.
                {console.log(questionResponse.responses)}
                    {questionResponse.responses.length > 0 && <div className="blue-bold ">{questionResponse.question_text}</div>}
                    {questionResponse.responses.length === 0 && questionResponse.question_text}
                    :
            </div>
            </div>
            <ul>
                {questionResponse.responses.map((response) =>
                    <li key={response.response_id}>{response.response_text}</li>
                )}
            </ul>
        </li>
    );

    function onClickUpdateConsultation() {
        localStorage.setItem('idUser', props.match.params.id)
    }

    return (
        <div className="case card mb-3">
            <h3 className="card-header">Consultation
           <Link to={`/consultation`}><button type="button" onClick={onClickUpdateConsultation} className="btn btn-info button-right">Update consultation</button></Link></h3>
            <div className="card-body margin-bot-15px">
                <ul className="list-group list-group-flush">
                    {displayQuestionResponses}
                </ul>
                <h4 className="thankyou-bold">Thank you</h4>
            </div>
        </div>
    )
}

export default UserConsultation;
