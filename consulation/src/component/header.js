import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem } from 'reactstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from '../page/login-page.js';
import Consultation from '../page/consultation.js';
import Consulation2 from '../page/consultation2.js';
import Consulation3 from '../page/consultation3.js';
import Consulation4 from '../page/consultation4.js';
import Logo from '../img/logo.png';
import Register from '../page/register.js';
import UserProfile from '../page/user_profile.js';
import UserConsultation from '../page/user_consultation.js';
import UserUpdate from '../page/user_update.js';

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render() {
    return (
      <Router>
        <Navbar className="navbar navbar-expand-lg navbar-light bg-light" >
          <img src={Logo} className="logo" alt="Logo" />
          <NavbarBrand className="navbar-brand" href="/consultation"> Marie-France Group</NavbarBrand>
          <NavbarToggler onClick={this.toggleNavbar} className="mr-2"/>

          <Collapse  isOpen={!this.state.collapsed} navbar>
            <Nav navbar className="navbar-nav mr-auto">
              <NavItem className="nav-item">
                <Link className="nav-link" to="/consultation">Consultation</Link>
              </NavItem>
              <NavItem className="nav-item">
                <Link className="nav-link" to="/register">Register</Link>
              </NavItem>
              <NavItem className="nav-item">
                <Link className="nav-link" to="/Login">Login</Link>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>

        <Route exact path="/consultation" component={Consultation} />
        <Route exact path="/" component={Consultation} />
        <Route path="/register" component={Register} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/consultation/2" component={Consulation2} />
        <Route exact path="/consultation/3" component={Consulation3} />
        <Route exact path="/consultation/4" component={Consulation4} />
        <Route exact path="/user/:id" component={UserProfile} />
        <Route exact path="/user/:id/consultation" component={UserConsultation} />
        <Route exact path="/user/:id/update" component={UserUpdate} />
      </Router>
    );
  }
}
