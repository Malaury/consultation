import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Label, FormGroup, Form, Input } from 'reactstrap';
import { Link } from 'react-router-dom'
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');

const UserUpdate = (props) => {
    const [user, setUser] = useState(null);
    useEffect(() => {
        jwtUtils.checkToken();
        axios.get(`${urls.URL_BACKEND}/users/${props.match.params.id}`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => setUser(res.data))
            .catch(error => console.log(error))
    }, [props]);

    function handleChange(event) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        })
    };

    function submit() {
        jwtUtils.checkToken();
        axios.put(`${urls.URL_BACKEND}/users/${props.match.params.id}`, user, {
            headers: {
                'token': localStorage.getItem('token')
            }
        });
    }

    return (
        <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
            <div className="card-header">Consultation</div>
            <div className="card-body">
                <Form>
                    <FormGroup>
                        <legend>Together we create</legend>
                        <FormGroup className="form-group">
                            <Label htmlFor="exampleInputEmail1">Email address *</Label>
                            <Input type="email" name="mail" className="form-control" value={user ? user.mail : ""} aria-describedby="emailHelp" placeholder={"Enter email"} onChange={handleChange} />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label>First name *</Label>
                            <Input type="text" name="firstname" className="form-control" value={user ? user.firstname : ""} placeholder={user ? user.firstname : "Enter your first name"} onChange={handleChange} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label>Last name *</Label>
                            <Input type="text" name="lastname" className="form-control" value={user ? user.lastname : ""} placeholder={"Enter your last name"} onChange={handleChange} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label >Phone *</Label>
                            <Input type="text" name="phone" className="form-control" value={user ? user.phone : ""} placeholder={"Enter your phone number"} onChange={handleChange} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <legend>Best contact *</legend>
                            <FormGroup check className="form-check">
                                <Label className="form-check-label">
                                    <Input name="bestcontact" type="radio" className="form-check-input" value="Phone"
                                        checked={(user) && (user.bestcontact === "Phone") ? "Phone" : ""} onChange={handleChange} />
                                    Phone
                                </Label>
                            </FormGroup>
                            <FormGroup className="form-check">
                                <Label className="form-check-label">
                                    <Input name="bestcontact" type="radio" className="form-check-input" value="Email"
                                        checked={(user) && (user.bestcontact === "Email") ? "Email" : ""} onChange={handleChange} />
                                    Email
                                </Label>
                            </FormGroup>
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label >Address</Label>
                            <Input type="text" className="form-control" name="address" value={user ? user.address : ""} placeholder={"Enter your address"} onChange={handleChange} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label >Suburb</Label>
                            <Input type="text" className="form-control" name="suburb" value={user ? user.suburb : ""} placeholder={"Enter your suburb"} onChange={handleChange} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label >State</Label>
                            <Input type="text" className="form-control" name="state" value={user ? user.state : ""} placeholder={"Enter your state"} onChange={handleChange} />
                        </FormGroup>
                        <FormGroup className="form-group">
                            <Label>Post code</Label>
                            <Input type="text" className="form-control" name="postalcode" value={user ? user.postalcode : "Enter your postal code"} placeholder={"Enter your postal code"} onChange={handleChange} />
                        </FormGroup>
                        <Input type="date" value={user && user.birthday ? user.birthday.substr(0, 10) : ""} name="birthday" onChange={handleChange} />
                    </FormGroup>
                </Form>
                <Link to="/register"><button onClick={submit} type="button" className="btn btn-primary">Update</button></Link>
            </div>
        </div>
    );
}

export default UserUpdate;