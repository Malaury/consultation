import React, { useState, useEffect } from 'react';
import { Form, Input, Button } from 'reactstrap';
import axios from 'axios';
import { Link } from 'react-router-dom';
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');
const RANGE_STEP = 30;

const UserList = () => {
    const [users, setUsers] = useState(null);
    var [range, setRange] = useState(RANGE_STEP);
    const [search, setSearch] = useState('');

    useEffect(() => {
        jwtUtils.checkToken();
        axios.get(`${urls.URL_BACKEND}/users`, {
            headers: {
                'token': localStorage.getItem('token'),
                'range': range
            }
        })
            .then(res => setUsers(res.data))
            .catch(error => console.log(error))
    }, []);

    function changeRange() {
        jwtUtils.checkToken();
        var rangeTemp = range + RANGE_STEP;
        setRange((range + RANGE_STEP));

        if (search === '') {
            axios.get(`${urls.URL_BACKEND}/users`, {
                headers: {
                    'token': localStorage.getItem('token'),
                    'range': rangeTemp
                }
            })
                .then(res => setUsers(res.data))
                .catch(error => console.log(error));
        }
        else {
            axios.get(`${urls.URL_BACKEND}/users/search/register`, {
                headers: {
                    'token': localStorage.getItem('token'),
                    'search': search,
                    'range': rangeTemp
                }
            })
                .then(res => setUsers(res.data))
                .catch(error => console.log(error))
        }
    }

    function searchByFirtsname(event) {
        jwtUtils.checkToken();
        setSearch(event.target.value);
        axios.get(`${urls.URL_BACKEND}/users/search/register`, {
            headers: {
                'token': localStorage.getItem('token'),
                'search': event.target.value,
                'range': range
            }
        })
            .then(res => setUsers(res.data))
            .catch(error => console.log(error))
    }

    const displayUsers = users ? users.map((user) =>
        <tr key={user.userid}>
            <th scope="row">{user.firstname}</th>
            <td>{user.lastname}</td>
            <td>{user.phone}</td>
            <td><Link to={`/user/${user.iduser}`}><button type="button" className="btn btn-info margin-left-15">Info</button></Link></td>
        </tr>
    ) : '';

    return (
        <div className="card-body"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></link>
            <h4 className="card-title">User list</h4>
            <Input className="form-control mr-sm-2" type="text" placeholder="Search" onChange={searchByFirtsname} />
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Firstname</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Phone</th>
                        <th scope="col">More Information</th>
                    </tr>
                </thead>
                <tbody>
                    {displayUsers}
                </tbody>
            </table>
            <button type="button" class="btn btn-secondary" onClick={changeRange}><i class="material-icons">vertical_align_bottom</i></button>
        </div>
    )
}
export default UserList;