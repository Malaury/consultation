import React from 'react';
import { Alert } from 'reactstrap';
import Axios from 'axios';
const urls = require('../utils/frontUtils');
const jwtUtils = require('../utils/jwtUtils');

export default class LoginPage extends React.Component{
    
    constructor(props){
        super(props)
        this.state={
            email:"",
            password:""
        }
        this.isLogin = jwtUtils.checkToken();
        this.errorLogin = false;
    }

    isLogin;
    errorLogin;

    valid(item, type)
    {
        let itemValue= item.target.value;
        switch(type)
        {
            case "email":{
                this.setState({email:itemValue});
                break;
            }
            case "password":{
                this.setState({password:itemValue});
                break;
            }
            default:{
                break;
            }
        }
    }

    submit(){
        const data ={
            email: `${this.state.email}`,
            password: `${this.state.password}`
        }
        this.errorLogin=true;
        Axios.post(`${urls.URL_BACKEND}/user-admin/login`, data).then(res => {
             localStorage.setItem('token',res.data.token);
             this.isLogin = true;
             this.errorLogin = false;
             localStorage.removeItem('idUser');
             this.forceUpdate();
        });
        this.forceUpdate();
    }

    disconnect(){
        localStorage.removeItem('token');
        this.isLogin=false;
        this.errorLogin = false;
        this.forceUpdate();
    }

    render(){
        let contentForm, contentLoged, contentError, contents;
        contentForm = ( <form>
                            <fieldset>
                                <div className="form-group">
                                    <label >Email address</label>
                                    <input type="email" className="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email" onChange={(item)=>this.valid(item, "email")}/>
                                </div>
                                <div className="form-group">
                                    <label >Password</label>
                                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={(item)=>this.valid(item, "password")}/>
                                </div>
                                <button  onClick={()=>this.submit()} type="button" className="btn btn-primary">Submit</button>
                            </fieldset>
                        </form> 
                      );

        contentLoged = (
            <div>
                <Alert color="success" className="alert alert-dismissible alert-success">
                    <strong>Well done!</strong> You are already connected!
                </Alert>
                <button type="button" onClick={()=>this.disconnect()} className="btn btn-danger">Disconnect</button>
            </div>
        );

        contentError = (
            <div className="margin-top-15px alert alert-dismissible alert-danger">
                <button type="button" className="close" data-dismiss="alert">&times;</button>
                Your login entries are false, try submitting again with a good e-mail and password.
            </div>
        );

        contents = contentForm;
        if(this.isLogin === true ){
           contents = contentLoged;
        }
        else if(this.errorLogin === true){
            contents = [contentError, contentForm];
        }
        return(
            <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
                <div className="card-header">Login</div>
                    <div className="card-body">
                        { contents }
                </div>
            </div>
        );
    }
}