const Joi = require('joi');

function validate(user_admin){
    const schema ={
        email: Joi.string().required(),
        password: Joi.string().required()
    };
    return Joi.validate(user_admin, schema);
}

exports.validate = validate;