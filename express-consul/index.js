const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
app.use(express.json());

app.use(cors());

// Body Parser configuration
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

const users = require('./api/user');
app.use('/users', users);

const user_has_question_response = require('./api/user_has_question_response');
app.use('/user_has_question_response', user_has_question_response);

const question = require('./api/question');
app.use('/question', question);

const response = require('./api/response');
app.use('/response', response);

const user_admin = require('./api/user_admin');
app.use('/user-admin',user_admin);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));

