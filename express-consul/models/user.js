const Joi = require('joi');

function validateUser(user){
    const schema = {
        firstname: Joi.string().required().replace("'","//'"),
        lastname: Joi.string().required(),
        phone: Joi.string().required(),
        bestcontact: Joi.string().min(5).max(5).optional().allow(null).allow(""),
        mail: Joi.string().optional().allow(null).allow(""),
        address: Joi.string().allow(null).optional().allow(""),
        suburb: Joi.string().allow(null).optional().allow(""),
        state: Joi.string().allow(null).optional().allow(""),
        postalcode: Joi.string().allow(null).optional().allow(""),
        birthday: Joi.date().allow("").allow(null),
        iduser: Joi.number().optional(),
        signature: Joi.string().allow("").allow(null).optional()
    };
    return Joi.validate(user, schema);
}

exports.validate = validateUser ;