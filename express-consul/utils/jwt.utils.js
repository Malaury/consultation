var jwt = require('jsonwebtoken');
const jwtDecode = require('jwt-decode');

const log = require('loglevel');
log.enableAll();

const JWT_SIGN_SECRET = 'cd6zuv24z68bwso5026scsc7aldid6843d694zp6ehqyugd5';

module.exports = {
    generateTokenForUser: function (idUser) {
        log.info("utils/jwt.utils - generateTokenForUser | Enterin /Finished function.");
        return jwt.sign({
            userId: idUser
        },
            JWT_SIGN_SECRET,
            {
                expiresIn: '12d'
            })
    },
    refreshTokenForUser: function (token) {
        log.info("utils/jwt.utils - refreshTokenForUser | Enter in function.");
        if (token === undefined || token === null){
            log.error("utils/jwt.utils - refreshTokenForUser |Token is null or undefined.");
            return false;
        } 
        if (jwt.verify(token, JWT_SIGN_SECRET, function (err, decoded) {
            if (err && err.name !== 'TokenExpiredError'){
                log.error("utils/jwt.utils - refreshTokenForUser | Error verify token: "+ err);
                return false;
            } 
            if (jwtDecode(token).userId != 1){
                log.error("utils/jwt.utils - refreshTokenForUser | This user is not allowed here: "+ jwtDecode(token).userId + ".");
                return false;
            } 
            return true;
        }) === false){
            log.error("utils/jwt.utils - refreshTokenForUser | Function finished with an error.");
            return false;
        } 
        log.info("utils/jwt.utils - refreshTokenForUser | Function finished.");
        return jwt.sign({
            userId: 1
        },
            JWT_SIGN_SECRET,
            {
                expiresIn: '12d'
            });;
    },
    decodeTokenForUser: function (token) {
        log.info("utils/jwt.utils - decodeTokenForUser | Enter in function.");
        if (token === undefined || token === null) return false;
        return jwt.verify(token, JWT_SIGN_SECRET, function (err, decoded) {
            if (err){
                log.info("utils/jwt.utils - decodeTokenForUser | Error verify token: "+ err);
                return false;
            } 
            if (jwtDecode(token).userId != 1){
                log.error("utils/jwt.utils - refreshTokenForUser | This user is not allowed here: "+ jwtDecode(token).userId + ".");
                return false;
            } 
            log.info("utils/jwt.utils - decodeTokenForUser | Function finished.");
            return true;
        });
    }
}