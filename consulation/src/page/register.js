import React from 'react';
import UserList from '../component/user_list';

export default class Register extends React.Component{

    render(){
        return(
            <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
                <div className="card-header">Register</div>
                    <UserList />
            </div>
        );
    }
}