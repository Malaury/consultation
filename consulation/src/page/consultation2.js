import React, { useState, useEffect } from 'react';
import { Label, FormGroup, Form, Input } from 'reactstrap';
import { checkResponse, otherResponse, checkSelect, updateTab, submitQuestionResponses } from '../utils/consultationUtils';
import axios from 'axios';
import { Message_EmptyUserID, Message_NeedToBeConnected, Message_UserIdentified } from '../component/message_consultation';
import image3 from '../img/consultation/3.png';
import image4 from '../img/consultation/4.jpg';
import image7 from '../img/consultation/5.JPG';
import Breakage from '../img/consultation/productConcerns/breakage.jpg';
import Dandruff from '../img/consultation/productConcerns/dandruff.jpg';
import Dry from '../img/consultation/productConcerns/dry.JPG';
import Dull from '../img/consultation/productConcerns/dull.jpg';
import Frizzy from '../img/consultation/productConcerns/frizzy.jpg';
import Limp from '../img/consultation/productConcerns/limp.jpg';
import Oily from '../img/consultation/productConcerns/oily.jpg';
import { Link, Redirect } from 'react-router-dom';
import SystemInput10 from'../component/systemRateInput10';
var cloneDeep = require('lodash.clonedeep');
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');

const Consultation2 = () => {
    const [user, setUser] = useState(null);
    const [login] = useState(jwtUtils.checkToken());
    const [questionResponses, setResponses] = useState(null);
    const [errodIdUser, setErrorIdUser] = useState(false);
    const [redirectConsultation, setRedirectConsultation] = useState(false);

    window.onbeforeunload = function(){ submitQuestionResponses(questionResponses, 1, 13)};

    useEffect(() => {
        window.scrollTo(0, 0);
        if (login === false) return;
        if(localStorage.getItem("idUser") === null || localStorage.getItem("idUser")=== ""){
            setErrorIdUser(true);
            return;
        }
        axios.get(`${urls.URL_BACKEND}/users/${localStorage.getItem("idUser")}`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                if(res.data === null){
                    console.log("enter in error");
                    setErrorIdUser(true);
                    return;
                }
                setUser(res.data);
            })
            .catch(error => console.log(error))

        axios.get(`${urls.URL_BACKEND}/user_has_question_response/${localStorage.getItem("idUser")}/consultation`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                setResponses(res.data);
            })
            .catch(error => console.log(error))
    }, []);

    function valid(itemValue, type, idQuestion) {
        setResponses(cloneDeep(updateTab(itemValue, type, idQuestion, questionResponses)));
    };

    function findImageKerastaseProduct(){
        if(!(questionResponses && questionResponses.length > 5 && questionResponses[5].responses && questionResponses[5].responses.length>0)) return Breakage;
        var firstResponseConcerns = questionResponses[5].responses[0].response_text;
        if(questionResponses[5].responses[0].response_type === 'text' && questionResponses[5].responses.length>1 )firstResponseConcerns = questionResponses[5].responses[1].response_text;
        if(firstResponseConcerns === "Uncontrollably curly") return Frizzy;
        else if(firstResponseConcerns === "Breakage") return Breakage;
        else if(firstResponseConcerns === "Frizzy") return Frizzy;
        else if( firstResponseConcerns === "Dry") return Dry;
        else if(firstResponseConcerns === "Oily") return Oily;
        else if(firstResponseConcerns === "Limp") return Limp;
        else if(firstResponseConcerns === "Dandruff") return Dandruff;
        else if(firstResponseConcerns === "Dull") return Dull;
        return Breakage;
    }
    //Content
    let contents, contentform;
    contentform = (
        <div > 
        {redirectConsultation && <Redirect to={'/consultation'}/>}
            <Form>
                <legend className="titleConsultation">Communication</legend>
                <img src={image3} className="image" alt="image3" />
                <FormGroup tag="fieldset" className="form-group">
                    <legend>Best time of day to contact you</legend>
                    <div class="container">
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio1" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 1)} value="Morning"
                                    checked={checkResponse("Morning", 1, questionResponses)} />{' '}
                                Morning
                        </Label>
                        </FormGroup>
                        <FormGroup check className="form-check" >
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio1" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 1)} value="Lunch" checked={checkResponse("Lunch", 1, questionResponses)} />{' '}
                                Lunch
                        </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio1" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 1)} value="Afternoon" checked={checkResponse("Afternoon", 1, questionResponses)} />{' '}
                                Afternoon
                        </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio1" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 1)} value="Evening" checked={checkResponse("Evening", 1, questionResponses)} />{' '}
                                Evening
                        </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend className="titleConsultation">I heard about Marie-France Group via</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-3">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Walk by" checked={checkResponse("Walk by", 2, questionResponses)}
                                        />{' '}
                                        Walk by
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Newspaper" checked={checkResponse("Newspaper", 2, questionResponses)}
                                        />{' '}
                                        Newspaper
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Radio" checked={checkResponse("Radio", 2, questionResponses)}
                                        />{' '}
                                        Radio
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Flyer" checked={checkResponse("Flyer", 2, questionResponses)}
                                        />{' '}
                                        Flyer
                            </Label>
                                </FormGroup>
                            </div>
                            <div class="col-3">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Magazine" checked={checkResponse("Magazine", 2, questionResponses)}
                                        />{' '}
                                        Magazine
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="TV" checked={checkResponse("TV", 2, questionResponses)}
                                        />{' '}
                                        TV
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Internet" checked={checkResponse("Internet", 2, questionResponses)}
                                        />{' '}
                                        Internet
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Gift voucher" checked={checkResponse("Gift voucher", 2, questionResponses)}
                                        />{' '}
                                        Gift voucher
                                    </Label>
                                </FormGroup>
                            </div>
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input type="radio" name="radio2" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 2)} value="Referral" checked={checkResponse("Referral", 2, questionResponses)}
                                        />{' '}
                                        Referral
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <FormGroup className="form-group">
                    <Label htmlFor="exampleTextarea">Please let us know who referred you to us, as we would love to thank them.</Label>
                    <Input type="textarea" className="form-control" onChange={(item) => valid(item.target.value, item.target.type, 3)}
                        value={questionResponses && questionResponses.length && questionResponses[2].responses && questionResponses[2].responses.length >0 ? questionResponses[2].responses[0]['response_text'] : null}
                        placeholder="Please write who referred you Marie-France Group."
                    />
                </FormGroup>
                <legend className="titleConsultation">My hair & scalp</legend>
                <img src={image4} className="image" alt="image4" />
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I think my hair is</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Fine" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Fine", 4, questionResponses)}
                                        />{' '}
                                        Fine
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Thick" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Thick", 4, questionResponses)}
                                        />{' '}
                                        Thick
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Coarse" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Coarse", 4, questionResponses)}
                                        />{' '}
                                        Coarse
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Curly" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Curly", 4, questionResponses)}
                                        />{' '}
                                        Curly
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Heavy" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Heavy", 4, questionResponses)}
                                        />{' '}
                                         Heavy
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Medium" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Medium", 4, questionResponses)}
                                        />{' '}
                                        Medium
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Straight" onChange={(item) => valid(item.target.value, item.target.type, 4)} type="checkbox" checked={checkResponse("Straight", 4, questionResponses)}
                                        />{' '}
                                        Straight
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I feel my scalp is</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Normal" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Normal", 5, questionResponses)}
                                        />{' '}
                                        Normal
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dandruff" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Dandruff", 5, questionResponses)}
                                        />{' '}
                                        Dandruff
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Itchy" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Itchy", 5, questionResponses)}
                                        />{' '}
                                        Itchy
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Psoriasis" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Psoriasis", 5, questionResponses)}
                                        />{' '}
                                        Psoriasis
                            </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Flakey" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Flakey", 5, questionResponses)}
                                        />{' '}
                                        Flakey
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Tight" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Tight", 5, questionResponses)}
                                        />{' '}
                                        Tight
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Hair loss" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Hair loss", 5, questionResponses)}
                                        />{' '}
                                        Hair loss
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Oily" onChange={(item) => valid(item.target.value, item.target.type, 5)} type="checkbox" checked={checkResponse("Oily", 5, questionResponses)}
                                        />{' '}
                                        Oily
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                    <div>
                        <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 5)} placeholder="Other" value={otherResponse(5,questionResponses)}
                        />
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I am concerned about my hair & scalp being</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dry" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Dry", 6, questionResponses)}
                                        />{' '}
                                        Dry
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Oily" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Oily", 6, questionResponses)}
                                        />{' '}
                                        Oily
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dull" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Dull", 6, questionResponses)}
                                        />{' '}
                                        Dull
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Limp" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Limp", 6, questionResponses)}
                                        />{' '}
                                        Limp
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Frizzy" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Frizzy", 6, questionResponses)}
                                        />{' '}
                                        Frizzy
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Breakage" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Breakage", 6, questionResponses)}
                                        />{' '}
                                        Breakage
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dandruff" onChange={(item) => valid(item.target.value, item.target.type, 6)} type="checkbox" checked={checkResponse("Dandruff", 6, questionResponses)}
                                        />{' '}
                                        Dandruff
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                    <div>
                        <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 6)} placeholder="Other" value={otherResponse(6, questionResponses)}
                        />
                    </div>
                </FormGroup>
                <FormGroup>
                    <Label htmlFor="exampleSelectMulti" className="questionConsultation">How commited are you to resolving this concern?</Label>
                    <SystemInput10 valueCharged={checkSelect(7, questionResponses)} callBack={valid} idQuestion={7} />
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I am having a bad day when my hair is</legend>
                    <div class="container">
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input className="form-check-input" value="Flat" onChange={(item) => valid(item.target.value, item.target.type, 8)} type="checkbox" checked={checkResponse("Flat", 8, questionResponses)} />{' '}
                                Flat
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input className="form-check-input" value="Curly" onChange={(item) => valid(item.target.value, item.target.type, 8)} type="checkbox" checked={checkResponse("Curly", 8, questionResponses)} />{' '}
                                Curly
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input className="form-check-input" value="Full" onChange={(item) => valid(item.target.value, item.target.type, 8)} type="checkbox" checked={checkResponse("Full", 8, questionResponses)} />{' '}
                                Full
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input className="form-check-input" value="Frizzy" onChange={(item) => valid(item.target.value, item.target.type, 8)} type="checkbox" checked={checkResponse("Frizzy", 8, questionResponses)} />{' '}
                                Frizzy
                            </Label>
                        </FormGroup>
                        <FormGroup>
                            <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 8)} placeholder="Other" value={otherResponse(8, questionResponses)} />
                        </FormGroup>
                    </div>
                </FormGroup>
                <div className="hl"/>
                <legend className="titleConsultation">Time for oneself</legend>
                <img src={image7} className="image" alt="image7" />
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I wash my hair</legend>
                    <div class="container">
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio9" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 9)} value="Daily" checked={checkResponse("Daily", 9, questionResponses)} />{' '}
                                Daily
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio9" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 9)} value="Every 2/3 days" checked={checkResponse("Every 2/3 days", 9, questionResponses)} />{' '}
                                Every 2/3 days
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio9" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 9)} value="Weekly" checked={checkResponse("Weekly", 9, questionResponses)} />{' '}
                                Weekly
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio9" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 9)} value="Other" checked={checkResponse("Other", 9, questionResponses)} />{' '}
                                Other
                            </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I spend on my hair (minutes)</legend>
                    <div class="container">
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio10" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 10)} value="5" checked={checkResponse("5", 10, questionResponses)} />{' '}
                                5
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio10" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 10)} value="10" checked={checkResponse("10", 10, questionResponses)} />{' '}
                                10
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio10" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 10)} value="20" checked={checkResponse("20", 10, questionResponses)} />{' '}
                                20
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label check className="form-check-label">
                                <Input type="radio" name="radio10" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 10)} value="Other" checked={checkResponse("Other", 10, questionResponses)} />{' '}
                                Other
                            </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <legend className="titleConsultation">Care</legend>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I am using</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Shampoo" onChange={(item) => valid(item.target.value, item.target.type, 11)} type="checkbox" checked={checkResponse("Shampoo", 11, questionResponses)} />{' '}
                                        Shampoo
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Conditioner" onChange={(item) => valid(item.target.value, item.target.type, 11)} type="checkbox" checked={checkResponse("Conditioner", 11, questionResponses)} />{' '}
                                        Conditioner
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Treatment" onChange={(item) => valid(item.target.value, item.target.type, 11)} type="checkbox" checked={checkResponse("Treatment", 11, questionResponses)} />{' '}
                                        Treatment
                            </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Spray" onChange={(item) => valid(item.target.value, item.target.type, 11)} type="checkbox" checked={checkResponse("Spray", 11, questionResponses)} />{' '}
                                        Spray
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Mousse" onChange={(item) => valid(item.target.value, item.target.type, 11)} type="checkbox" checked={checkResponse("Mousse", 11, questionResponses)} />{' '}
                                        Mousse
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Wax" onChange={(item) => valid(item.target.value, item.target.type, 11)} type="checkbox" checked={checkResponse("Wax", 11, questionResponses)} />{' '}
                                        Wax
                            </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                    <div>
                        <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 11)} placeholder="Other" value={otherResponse(11, questionResponses)} />
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I select my products for the following hair type / concerns</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-4">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Sun" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Sun", 12, questionResponses)} />{' '}
                                        Sun
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Heat" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Heat", 12, questionResponses)} />{' '}
                                        Heat
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Fine/Limp" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Fine/Limp", 12, questionResponses)} />{' '}
                                        Fine/Limp
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Rebellious/Frizzy" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Rebellious/Frizzy", 12, questionResponses)} />{' '}
                                        Rebellious/Frizzy
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dry" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Dry", 12, questionResponses)} />{' '}
                                        Dry
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Volume" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Volume", 12, questionResponses)} />{' '}
                                        Volume
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Coloured" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Coloured", 12, questionResponses)} />{' '}
                                        Coloured
                            </Label>
                                </FormGroup>
                            </div>
                            <div className="col-4">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Damaged" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Damaged", 12, questionResponses)} />{' '}
                                        Damaged
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Density" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Density", 12, questionResponses)} />{' '}
                                        Density
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Oily" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Oily", 12, questionResponses)} />{' '}
                                        Oily
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dandruff" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Dandruff", 12, questionResponses)} />{' '}
                                        Dandruff
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Uncontrollable curly" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Uncontrollable curly", 12, questionResponses)} />{' '}
                                        Uncontrollable curly
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Normal" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Normal", 12, questionResponses)} />{' '}
                                        Normal
                            </Label>
                                </FormGroup>
                            </div>
                            <div className="col-4">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Hair loss" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Hair loss", 12, questionResponses)} />{' '}
                                        Hair loss
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Breakage" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Breakage", 12, questionResponses)} />{' '}
                                        Breakage
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Sensitive/Itchy/Tight" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Sensitive/Itchy/Tight", 12, questionResponses)} />{' '}
                                        Sensitive/Itchy/Tight
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Heat protection" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Heat protection", 12, questionResponses)} />{' '}
                                        Heat protection
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Natural ingredients" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Natural ingredients", 12, questionResponses)} />{' '}
                                        Natural ingredients
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Scalp issues" onChange={(item) => valid(item.target.value, item.target.type, 12)} type="checkbox" checked={checkResponse("Scalp issues", 12, questionResponses)} />{' '}
                                        Scalp issues
                            </Label>
                                </FormGroup>
                            
                            </div>
                        </div>
                        <FormGroup>
                            <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 12)} placeholder="Other" value={otherResponse(12, questionResponses)} />
                        </FormGroup>
                    </div>
                </FormGroup>
                <img src={findImageKerastaseProduct()} className="image" alt="product keratase" />
                <FormGroup tag="fieldset" className="form-group">
                    <legend>In the morning I use</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-3">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Dryer" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Dryer", 13, questionResponses)} />{' '}
                                        Dryer
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Brush" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Brush", 13, questionResponses)} />{' '}
                                        Brush
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Diffuser" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Diffuser", 13, questionResponses)} />{' '}
                                        Diffuser
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Fingers" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Fingers", 13, questionResponses)} />{' '}
                                        Fingers
                            </Label>
                                </FormGroup>
                            </div>
                            <div className="col-3">
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Curling iron" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Curling iron", 13, questionResponses)} />{' '}
                                        Curling iron
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Hot rollers" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Hot rollers", 13, questionResponses)} />{' '}
                                        Hot rollers
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Flat iron" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Flat iron", 13, questionResponses)} />{' '}
                                        Flat iron
                            </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label check className="form-check-label">
                                        <Input className="form-check-input" value="Comb" onChange={(item) => valid(item.target.value, item.target.type, 13)} type="checkbox" checked={checkResponse("Comb", 13, questionResponses)} />{' '}
                                        Comb
                            </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                    <div>
                        <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 13)} placeholder="Other" value={otherResponse(13,questionResponses)} />
                    </div>
                </FormGroup>
            </Form>
            <Link to="/consultation"><button type="button" onClick={() => submitQuestionResponses(questionResponses, 1, 13)} className="btn btn-secondary">Previous</button></Link>
            <Link to="/consultation/3"><button type="button" onClick={() => submitQuestionResponses(questionResponses, 1, 13)} className="btn btn-secondary">Next</button></Link>
        </div>
    );
    contents = [<Message_UserIdentified user={user} callBack={setRedirectConsultation}/>, contentform];
    if (login === false) contents = Message_NeedToBeConnected();
    if (errodIdUser === true) contents = Message_EmptyUserID();

    return (
        <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
            <div className="card-header">Hair condition</div>
            <div className="card-body">
                {contents}
            </div>
        </div>
    );
}

export default Consultation2;