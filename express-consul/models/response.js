const Joi = require('joi');

function validateResponse(response){
    const schema = {
        text: Joi.string().required(),
    };
    return Joi.validate(response, schema);
}

exports.validate = validateResponse;