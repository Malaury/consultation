const { validate } = require('../models/user');
const express = require('express');
const router = express.Router();
const db = require('../utils/mysqldb');
const jwtUtils = require('../utils/jwt.utils');
const sendEmails = require('../utils/sendEmail');
const log = require('loglevel');
log.enableAll();

router.get('/', (req, res) => {
    log.info("api/user - get('/') | Enter in function.");
    var token = req.headers.token;
    const range = parseInt(req.headers.range);
    if (token === undefined || token === null) return res.status(400).send("Error 400 - A token is needed");
    if (jwtUtils.decodeTokenForUser(token) === false) return res.status(403).send("Error 403 - Access refused.");
    if(range === undefined || range ===null) return res.status(400).send("Error 400 - A range is needed.");
    const queryString = "SELECT * FROM user ORDER BY `iduser` DESC LIMIT ?"
    db.connection.query(queryString, [range], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
    log.info("api/user - get('/') | Function finished.");
});

async function getUserIdMax() {
    log.info("api/user - getUserNumber | Enter in function.");
    return new Promise((resolve, reject) => {
        const queryString = "SELECT * FROM user ORDER BY `iduser` DESC"
        db.connection.query(queryString, (err, rows) => {
            if (err) reject(err);
            log.info("api/user - getUserNumber | " + rows.length + "user(s) found.");
            log.info("api/user - getUserNumber | Function finished.");
            resolve(rows[0].iduser);
        });
    })
}

router.post('/', async (req, res) => {
    log.info("api/user - post('/') | Enter in function.");
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const idUser = await getUserIdMax() + 1;
    const user = ({
        iduser: idUser,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        phone: req.body.phone,
        mail: req.body.mail === undefined ? '' : req.body.mail,
        address: req.body.address === undefined ? '' : req.body.address,
        suburb: req.body.suburb === undefined ? '' : req.body.suburb,
        state: req.body.state === undefined ? '' : req.body.state,
        postalcode: req.body.postalcode === undefined ? '' : req.body.postalcode,
        birthday: req.body.birthday === undefined ? '' : req.body.birthday,
        bestcontact: req.body.bestcontact === undefined ? '' : req.body.bestcontact
    });
    if (user.birthday === undefined || user.birthday === null || user.birthday === "") user.birthday = '2019-01-01';
    const queryString = `INSERT INTO user (\`iduser\`,\`firstname\`, \`lastname\`, \`phone\`, \`mail\`, \`address\`, \`suburb\`, \`state\`, \`postalcode\`, \`birthday\`, \`bestcontact\`) VALUES (?, ? , ?, ?, ?, ?, ?, ?, ?, ?, ?);`
    db.connection.query(queryString, [user.iduser,user.firstname,user.lastname,user.phone,user.mail,user.address,user.suburb,user.state,user.postalcode,user.birthday,user.bestcontact], (err, rows) => {
        if (err) throw err;
        log.info("api/user - post('/') | Post the user: ");
        log.info(user);
        log.info("api/user - post('/') | Send email.");
        sendEmails.sendNewUserEmail(user);
    });
    res.send(user);
    log.info("api/user - post('/') | Function finished.");
});

router.get('/:id', (req, res) => {
    log.info("api/user - get('/:id') | Enter in function.");
    if (jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    if (isNaN(req.params.id)) return res.status(400).send("Error 400 - The param id need to be a decimal");
    const queryString = `SELECT * FROM user WHERE iduser=?`
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
        log.info("api/user - get('/:id') | User found: " + rows[0] + ".");
        res.json(rows[0]);
    });
    log.info("api/user - get('/:id') | Function finished.");
});

router.put('/signature/:id', (req, res) => {
    log.info("api/user - put('/signature/:id') | Enter in function.");
    if (jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    if (isNaN(req.params.id)) return res.status(400).send("Error 400 - The param id need to be a decimal");
    let signature = (req.body.signature);
    if(!signature) return res.status(400).send("Error 400 -  A signature data is needed.");
    
    const queryString = `UPDATE user SET \`signature\` = ? WHERE (\`iduser\` = ?);`
    db.connection.query(queryString, [signature, req.params.id], (err, rows) => {
        if(err) throw err;
        res.send(signature);
    });
    log.info("api/user - put('/signature/:id') | Function finished");
});

router.get('/search/register', (req, res) => {
    log.info("api/user - get('/search/register') | Enter in function.");
    const range = parseInt(req.headers.range);
    if (jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    if(range === undefined || range ===null) return res.status(400).send("Error 400 - A range is needed.");
    let search = (req.headers.search === undefined) ? "" : req.headers.search;
    search = '%' + search +'%';
    const queryString = `SELECT * FROM user WHERE \`firstname\` LIKE ? OR \`lastname\` LIKE ? OR \`phone\` LIKE ? OR \`mail\` LIKE ? ORDER BY \`iduser\` DESC LIMIT ?`
    db.connection.query(queryString, [search,search,search,search,range], (err, rows) => {
        if (err) throw err;
        log.info("api/user - get('/search/register') | " + rows.length + "user found for this search.");
        res.json(rows);
    });
    log.info("api/user - get('/search/register') | Function finished.");
});

router.get('/phone/:phone', (req, res) => {
    log.info("api/user - get('/phone/:phone') | Enter in function.");
    if (jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const queryString = 'SELECT * FROM user WHERE \`phone\` = ?'
    db.connection.query(queryString, [req.params.phone], (err, rows) => {
        if (err) throw err;
        log.info("api/user - get('/phone/:phone') | " + rows.length + "found with this phone: " + req.params.phone + ".");
        res.json(rows);
    });
    log.info("api/user - get('/phone/:phone') | Function finished.");
});

function getUerId(id) {
    log.info("api/user - getUserId | Enter in function.");
    return new Promise((resolve, reject) => {
        const queryString = `SELECT * FROM user WHERE iduser=?`
        db.connection.query(queryString,[id], (err, rows) => {
            if (err) reject(err);
            log.info("api/user - getUserId | User found: " + rows[0] + ".");
            log.info("api/user - getUserId | Function finished.");
            resolve(rows[0]);
        });
    })
}

router.put('/:id', async (req, res) => {
    log.info("api/user - get('/:id') | Enter in function.");
    const { error } = validate(req.body);
    if (error){
        console.log(error)
         return res.status(400).send(error.details[0].message);
    }
   
    if (jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const user = ({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        phone: req.body.phone,
        mail: req.body.mail,
        address: req.body.address,
        suburb: req.body.suburb,
        state: req.body.state,
        postalcode: req.body.postalcode,
        birthday: req.body.birthday,
        bestcontact: req.body.bestcontact
    });

    const userFoundInDB = await getUerId(req.params.id);
    let difference = [];
    if(user.firstname!== userFoundInDB.firstname) difference.push("firstname");
    if(user.lastname!== userFoundInDB.lastname) difference.push("lastname");
    if(user.phone!== userFoundInDB.phone) difference.push("phone");
    if(user.address!== userFoundInDB.address) difference.push("address");
    if(user.suburb!== userFoundInDB.suburb) difference.push("suburb");
    if(user.mail!== userFoundInDB.mail) difference.push("mail");
    if(user.state!== userFoundInDB.state) difference.push("state");
    if(user.postalcode!== userFoundInDB.postalcode) difference.push("postalcode");

    if (user.birthday === undefined) user.birthday = '0000-00-00';
    const queryString = `UPDATE user SET \`firstname\` = ?, \`lastname\` = ?, \`phone\` = ?,
     \`mail\` = ?, \`address\` = ?, \`suburb\` = ?, \`state\` = ?, \`postalcode\` = ?,
     \`birthday\` = ?, \`bestcontact\` = ? WHERE (\`iduser\` = ?);`
    db.connection.query(queryString, [user.firstname,user.lastname,user.phone,user.mail,user.address,user.suburb,user.state,user.postalcode,user.birthday.substr(0, 10),user.bestcontact,req.params.id], (err, rows) => {
        if (err) throw err;
        log.info("api/user - get('/:id') | User update: " + user + ".");
        log.info("api/user - get('/:id') | Send Email.");
        sendEmails.sendUpdateUserEmail(user, difference);
    });
    res.send(user);
    log.info("api/user - get('/:id') | Function finished.");
});

module.exports = router;