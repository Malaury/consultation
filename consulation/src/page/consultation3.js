import React, { useState, useEffect } from 'react';
import { Label, FormGroup, Form, Input, Button } from 'reactstrap';
import { checkResponse, otherResponse, checkSelect, updateTab, submitQuestionResponses } from '../utils/consultationUtils';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import { Message_EmptyUserID, Message_NeedToBeConnected, Message_UserIdentified } from '../component/message_consultation';
import image9 from '../img/consultation/6.jpg';
import image10 from '../img/consultation/7.jpg';
import SystemInput10 from'../component/systemRateInput10';
var cloneDeep = require('lodash.clonedeep');
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');


const Consultation3 = () => {
    const [user, setUser] = useState(null);
    const [login] = useState(jwtUtils.checkToken());
    let [questionResponses, setResponses] = useState(null);
    const [errodIdUser, setErrorIdUser] = useState(false);
    const [redirectConsultation, setRedirectConsultation] = useState(false);

    window.onbeforeunload = function(){ submitQuestionResponses(questionResponses, 14, 23)};

    useEffect(() => {
        window.scrollTo(0, 0);
        if (login === false) return;
        if (localStorage.getItem("idUser") === null || localStorage.getItem("idUser") === "") {
            setErrorIdUser(true);
            return;
        }
        axios.get(`${urls.URL_BACKEND}/users/${localStorage.getItem("idUser")}`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                if (res.data === null) {
                    console.log("enter in error");
                    setErrorIdUser(true);
                    return;
                }
                setUser(res.data);
            })
            .catch(error => console.log(error))

        axios.get(`${urls.URL_BACKEND}/user_has_question_response/${localStorage.getItem("idUser")}/consultation`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                setResponses(res.data);
            })
            .catch(error => console.log(error))
    }, []);

    function valid(itemValue, type, idQuestion) {
        setResponses(cloneDeep(updateTab(itemValue, type, idQuestion, questionResponses)));
    };

    let contentform, contents;
    contentform = (
        <Form>
            {redirectConsultation && <Redirect to={'/consultation'} />}
            <legend className="titleConsultation">Design & Colour Vision</legend>
            <legend >Design</legend>
            <img src={image9} className="image" alt="image9" />
            <FormGroup tag="fieldset" className="form-group">
                <div class="container">
                    <FormGroup>
                        <Label className="questionConsultation" for="exampleSelect1">How satisfied  are you with your current design ?</Label>
                        <SystemInput10 valueCharged={checkSelect(14, questionResponses)} callBack={valid} idQuestion={14} />
                    </FormGroup>
                </div>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I feel my haircut is</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 15)} type="checkbox" value="Dated" checked={checkResponse("Dated", 15, questionResponses)} />
                                        Dated
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 15)} type="checkbox" value="Does not suit the shape of my face" checked={checkResponse("Does not suit the shape of my face", 15, questionResponses)} />
                                        Doesn't suit the shape of my face
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 15)} type="checkbox" value="Too long" checked={checkResponse("Too long", 15, questionResponses)} />
                                        Too long
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-6">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 15)} type="checkbox" value="Too short" checked={checkResponse("Too short", 15, questionResponses)} />
                                        Too short
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 15)} type="checkbox" value="Great" checked={checkResponse("Great", 15, questionResponses)} />
                                        Great
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                    <FormGroup check className="form-check">
                        <Input type="text" className="form-control margin-top-15px" onChange={(item) => valid(item.target.value, item.target.type, 15)} placeholder="Other" value={otherResponse(15, questionResponses)} />
                    </FormGroup>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I feel like a change for my haircut</legend>
                    <div class="container">
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 16)} name="radio16" value="Yes" checked={checkResponse("Yes", 16, questionResponses)} />
                                Yes
                                    </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 16)} name="radio16" value="No" checked={checkResponse("No", 16, questionResponses)} />
                                No
                                    </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I would like my hair to be:</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 17)} value="Long" checked={checkResponse("Long", 17, questionResponses)} />
                                        Long
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 17)} value="Short" checked={checkResponse("Short", 17, questionResponses)} />
                                        Short
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 17)} value="Straight" checked={checkResponse("Straight", 17, questionResponses)} />
                                        Straight
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 17)} value="Curly" checked={checkResponse("Curly", 17, questionResponses)} />
                                        Curly
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 17)} value="Shiny" checked={checkResponse("Shiny", 17, questionResponses)} />
                                        Shiny
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 17)} value="Maintained" checked={checkResponse("Maintained", 17, questionResponses)} />
                                        Maintained
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <div className="hl" />
                <Label className="titleConsultation">Colour</Label>
                <img src={image10} className="image" alt="image10" />
                <div class="container">
                    <FormGroup tag="fieldset" className="form-group">
                        <legend>How satisfied are you with your hair colour ?</legend>
                        <SystemInput10 valueCharged={checkSelect(18, questionResponses)} callBack={valid} idQuestion={18} />
                    </FormGroup>
                </div>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I feel my hair colour is</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 19)} value="Great" checked={checkResponse("Great", 19, questionResponses)} />
                                        Great
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 19)} value="Boring" checked={checkResponse("Boring", 19, questionResponses)} />
                                        Boring
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 19)} value="Lifeless" checked={checkResponse("Lifeless", 19, questionResponses)} />
                                        Lifeless
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-6">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 19)} value="Fades" checked={checkResponse("Fades", 19, questionResponses)} />
                                        Fades
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" onChange={(item) => valid(item.target.value, item.target.type, 19)} value="Does not suit my complexion" checked={checkResponse("Does not suit my complexion", 19, questionResponses)} />
                                        Doesn't suit my complexion
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I feel like a change for my hair colour</legend>
                    <div class="container">
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 20)} name="radio20" value="Yes" checked={checkResponse("Yes", 20, questionResponses)} />
                                Yes
                                    </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 20)} name="radio20" value="No" checked={checkResponse("No", 20, questionResponses)} />
                                No
                                    </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I would like my colour to be</legend>
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Lighter" checked={checkResponse("Lighter", 21, questionResponses)} />
                                        Lighter
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Darker" checked={checkResponse("Darker", 21, questionResponses)} />
                                        Darker
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Cooler" checked={checkResponse("Cooler", 21, questionResponses)} />
                                        Cooler
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Warmer" checked={checkResponse("Warmer", 21, questionResponses)} />
                                        Warmer
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-4">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Contrast" checked={checkResponse("Contrast", 21, questionResponses)} />
                                        Contrast
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Maintained" checked={checkResponse("Maintained", 21, questionResponses)} />
                                        Maintained
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Shinner" checked={checkResponse("Shinner", 21, questionResponses)} />
                                        Shinner
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 21)} type="checkbox" value="Not applicable" checked={checkResponse("Not applicable", 21, questionResponses)} />
                                        Not applicable
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>Do you have any known allergies or past reactions to colour</legend>
                    <div class="container">
                        <div className="row">
                            <div className="col-3">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 22)} name="radio22" value="Yes" checked={checkResponse("Yes", 22, questionResponses)} />
                                        Yes
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 22)} name="radio22" value="No" checked={checkResponse("No", 22, questionResponses)} />
                                        No
                                    </Label>
                                </FormGroup>
                            </div>
                            {checkResponse("Yes", 22, questionResponses) !== null &&
                            <div className="col-9">
                                <FormGroup check className="form-check">
                                    <Input type="text" onChange={(item) => valid(item.target.value, item.target.type, 22)} placeholder="Could you write more details about your problem ?" value={otherResponse(22, questionResponses)} />
                                </FormGroup>
                            </div>
                            }
                        </div>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                        <Label className="questionConsultation" for="exampleSelect1">How often would you like to maintain your colour & cuts? (Weeks)</Label>
                        <Input type="select" className="form-control" onChange={(item) => valid(item.target.value, item.target.type, 23)} value={checkSelect(23, questionResponses)}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </Input>
                </FormGroup>
            </FormGroup>
            <Link to="/consultation/2"><button type="button" onClick={() => submitQuestionResponses(questionResponses, 14, 23)} className="btn btn-secondary">Previous</button></Link>
            <Link to="/consultation/4"><button type="button" onClick={() => submitQuestionResponses(questionResponses, 14, 23)} className="btn btn-secondary">Next</button></Link>
        </Form>
    );
    contents = [<Message_UserIdentified user={user} callBack={setRedirectConsultation} />, contentform];
    if (login === false) contents = Message_NeedToBeConnected();
    if (errodIdUser === true) contents = Message_EmptyUserID();

    return (
        <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
            <div className="card-header">Design & Colour Vision</div>
            <div className="card-body">
                {contents}
            </div>
        </div>
    );
}
export default Consultation3;