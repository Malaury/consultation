const jwtDecode = require('jwt-decode');
const axios = require('axios');
const urls = require('../utils/frontUtils');

module.exports = {
    checkToken: function(){
        console.log("eneter")
        var token = localStorage.getItem('token');
        if(token === undefined || token === null) return false;
        if((jwtDecode(token).exp < Date.now() /1000)){
            console.log("enter here")
            // Token expired, refresh Token
            axios.get(`${urls.URL_BACKEND}/user-admin/refresh`, { headers: {
                'token': `${localStorage.getItem('token')}`
            }})
                .then(res => {
                    if(res.status === 200){
                        localStorage.setItem('token', res.data.token);
                        return true;
                    }
                })
                .catch( function (error) {
                    localStorage.removeItem('token');
                    return false;
                });
        }
        return true;
    }
}