import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');

const UserProfile = (props) => {
    const [user, setUser] = useState(null);
    useEffect(() => {
        jwtUtils.checkToken();
        axios.get(`${urls.URL_BACKEND}/users/${props.match.params.id}`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => setUser(res.data))
            .catch(error => console.log(error))
    }, [props]);


    return (
        <div className="case card mb-3">
            <h3 className="card-header">profile</h3>
            <div className="card-body">
                <h5 className="card-title">{user ? user.firstname : ''} {user ? user.lastname : ''}</h5>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">Firstnam : {user ? user.firstname : ''}</li>
                    <li className="list-group-item">Lastname : {user ? user.lastname : ''}</li>
                    <li className="list-group-item">Phone : {user ? user.phone : ''}</li>
                    <li className="list-group-item">Mail : {user ? user.mail : ''}</li>
                    <li className="list-group-item">Best contact : {user ? user.bestcontact : ''}</li>
                    <li className="list-group-item">Address : {user ? user.address : ''}</li>
                    <li className="list-group-item">Suburb : {user ? user.suburb : ''}</li>
                    <li className="list-group-item">State : {user ? user.state : ''}</li>
                    <li className="list-group-item">Postal code : {user ? user.postalcode : ''}</li>
                    <li className="list-group-item">Birthday : {user ? user.birthday.substr(0, 10) : ''}</li>
                </ul>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <Link to={`/user/${user ? user.iduser : ''}/consultation`}><button type="button" className="btn btn btn-info margin-bot-15px margin-left-5">Consultation</button></Link>
                    </div>
                    <div className="col-5">
                        <Link to={`/consultation`}><button type="button" onClick={user ? localStorage.setItem('idUser', user.iduser) : ''} className="btn btn btn-info margin-bot-15px margin-left-16">Update consultation</button></Link>
                    </div>
                    <div className="col-4">
                        <Link to={`/user/${user ? user.iduser : ''}/update`}><button type="button" className="btn btn  margin-bot-15px btn-info">Update User</button></Link>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default UserProfile;