import axios from 'axios';
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');

function checkResponse(expectedValue, questionId, questionResponses) {
    if(questionId === 36 && questionResponses && questionResponses.length && questionResponses[35] ) console.log(questionResponses[35]);
    questionId--;
    if (questionResponses && questionResponses.length && questionResponses[questionId] && questionResponses[questionId].responses && questionResponses[questionId].responses.find(r => r.response_text === expectedValue) !== undefined) {
        return "checked";
    }
    return null;
}

function otherResponse(questionId, questionResponses) {
    questionId--;
    return questionResponses && questionResponses.length && questionResponses[questionId] && questionResponses[questionId].responses && questionResponses[questionId].responses.find(r => r.response_type === 'text') ? questionResponses[questionId].responses.find(r => r.response_type === 'text').response_text : null;
}

function checkSelect(questionId, questionResponses) {
    questionId--;
    return questionResponses && questionResponses.length && questionResponses[questionId] && questionResponses[questionId].responses && questionResponses[questionId].responses && questionResponses[questionId].responses[0] ? questionResponses[questionId].responses[0].response_text : null;
}

function updateTab(itemValue, type, idQuestion, questionResponses) {
    idQuestion--;
    let updateTab = questionResponses;
    if (!updateTab || !updateTab[idQuestion] || !updateTab[idQuestion].responses) return;
    if (type === "radio" || type === "textarea" || type === 'select-one') {
        if(idQuestion===21){
            updateTab[idQuestion].responses = updateTab[idQuestion].responses.filter(r => r.response_type === 'radio');
        }
        if (itemValue === "") {
            updateTab[idQuestion].responses = questionResponses[idQuestion].responses = [];
            return updateTab;
        }
        updateTab[idQuestion].responses[0] = {
            response_text: itemValue,
            response_type: type,
        };
    }
    else if (type === "checkbox") {
        if (updateTab[idQuestion].responses.find(r => r.response_text === itemValue) !== undefined) {
            updateTab[idQuestion].responses = updateTab[idQuestion].responses.filter(r => r.response_text !== itemValue);
        }
        else {
            updateTab[idQuestion].responses.push({
                response_text: itemValue,
                response_type: type,
            });
        }
    }
    else if (type === 'text') {
        if (updateTab[idQuestion].responses.find(r => r.response_type === 'text') !== undefined) {
            updateTab[idQuestion].responses = updateTab[idQuestion].responses.filter(r => r.response_type !== 'text');
            if (itemValue !== "") {
                updateTab[idQuestion].responses.push({
                    response_text: itemValue,
                    response_type: type,
                });
            }
        }
        else {
            updateTab[idQuestion].responses.push({
                response_text: itemValue,
                response_type: type,
            });
        }
    }
    console.log(updateTab)
    return updateTab;
}

function questionResponsesParseToSendModel(questionResponses) {
    let tab = [];
    if(questionResponses === null || questionResponses === undefined) return tab;
    questionResponses.forEach(questionResponse => {
        questionResponse.responses.forEach(response => {
            tab.push({
                question_id: questionResponse.question_id,
                response_text: response.response_text,
                type: response.response_type
            })
        });
    });
    console.table(tab);
    return tab;
}

async function submitQuestionResponses(questionResponses, idMin, idMax) {
    console.log(questionResponses);
    jwtUtils.checkToken();
    const data = {
        questionResponseTab: questionResponsesParseToSendModel(questionResponses)
    }
    await axios.post(`${urls.URL_BACKEND}/user_has_question_response/${localStorage.getItem("idUser")}/${idMin}/${idMax}`, data, {
        headers: {
            'token': localStorage.getItem('token')
        }
    });
    console.log("eheo")
}

function checkDisabled(questionResponses, verificationBool, idQuestion, itemValue){
    idQuestion--;
    if(questionResponses === undefined || questionResponses === null || questionResponses[idQuestion] === null || questionResponses[idQuestion] === undefined 
        || questionResponses[idQuestion].responses === null || questionResponses[idQuestion].responses === undefined || questionResponses[idQuestion].responses.length === 0) return "";
    if(verificationBool === false){
        if(questionResponses[idQuestion].responses.find(r => r.response_text === itemValue) !== undefined) return "";
        return "disabled";
    }
    return "";
}

function checkDisabledService(questionResponses, verificationBool, itemValue){
    if(questionResponses === undefined || questionResponses === null || questionResponses[29] === null || questionResponses[29] === undefined 
        || questionResponses[29].responses === null || questionResponses[29].responses === undefined ) return "";
    if(questionResponses[30] === null || questionResponses[30] === undefined 
        || questionResponses[30].responses === null || questionResponses[30].responses === undefined ) return ""; 
    if(questionResponses[31] === null || questionResponses[31] === undefined 
         || questionResponses[31].responses === null || questionResponses[31].responses === undefined ) return "";

    if(verificationBool === false){
        if((questionResponses[29].responses.find(r=> r.response_text === itemValue) !== undefined)|| 
        (questionResponses[30].responses.find(r=> r.response_text === itemValue) !== undefined) ||
        (questionResponses[31].responses.find(r=> r.response_text === itemValue) !== undefined) ) return "";
        return "disabled";
    }
    return "";
}
export { checkResponse, otherResponse, checkSelect, updateTab, submitQuestionResponses,checkDisabled, checkDisabledService }