const express = require('express');
const router = express.Router();
const jwtUtils = require('../utils/jwt.utils');
const db = require('../utils/mysqldb');
const mysql = require('mysql');

const log = require('loglevel');
log.enableAll();

router.get('/:id', (req, res) => {
    log.info("api/question - get('/id') | Enter in function.");
    if (jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const queryString = `SELECT * FROM question WHERE idquestion=?`
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
        res.json(rows);
        log.info("api/question - get('/:id') | Get question data by idQuestion " + req.params.id + ": " + rows + ".");
    });
    log.info("api/question - get('/id') | Funstion finished.");
});

module.exports = router;