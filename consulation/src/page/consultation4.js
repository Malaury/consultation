import React, { useState, useEffect, useRef } from 'react';
import { Label, FormGroup, Form, Input, Button } from 'reactstrap';
import { checkResponse, otherResponse, updateTab, submitQuestionResponses, checkDisabled, checkDisabledService } from '../utils/consultationUtils';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Message_EmptyUserID, Message_NeedToBeConnected, Message_UserIdentified } from '../component/message_consultation';
import image14 from '../img/consultation/8.jpg';
import shape from '../img/consultation/shape.JPG';
import image15 from '../img/consultation/9.jpg';
import lot1 from '../img/consultation/lot1.JPG';
import lot2 from '../img/consultation/lot2.JPG';
import lot3 from '../img/consultation/lot3.JPG';
import image10 from '../img/consultation/10.jpg';
import SignaturePad from 'react-signature-canvas';
import { Redirect } from 'react-router-dom';
import { timingSafeEqual } from 'crypto';
const jwtUtils = require('../utils/jwtUtils');
var cloneDeep = require('lodash.clonedeep');
const urls = require('../utils/frontUtils');

const Consultation4 = () => {
    const [user, setUser] = useState(null);
    const [login] = useState(jwtUtils.checkToken());
    let [questionResponses, setResponses] = useState(null);
    const [questionResponsesVerification, setQuestionResponseVerification] = useState({
        iHaveConcernsAbout: true,
        questionService: true
    });
    const [errodIdUser, setErrorIdUser] = useState(false);
    const [redirect, setRedirect] = useState(false);
    const [redirectConsultation, setRedirectConsultation] = useState(false);
    var sigPad = '{}';
    var [trimmedDataUrl, setTrimmedDataUrl] = useState(null);

    window.onbeforeunload = function(){ submitQuestionResponses(questionResponses, 24, 35)};

    const updateSignature = () => {
        setTrimmedDataUrl(null);
    }

    const clear = () => {
        sigPad.clear();
    }

    const trim = () => {
        if (sigPad === "{}" || sigPad === null) return;
        setTrimmedDataUrl(sigPad.getTrimmedCanvas().toDataURL('image/png'));
        const data = {
            signature: sigPad.getTrimmedCanvas().toDataURL('image/png')
        }
        axios.put(`${urls.URL_BACKEND}/users/signature/${localStorage.getItem("idUser")}`, data, {
            headers: {
                'token': localStorage.getItem('token')
            }
        });
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        if (login === false) return;
        if (localStorage.getItem("idUser") === null || localStorage.getItem("idUser") === "") {
            setErrorIdUser(true);
            return;
        }
        axios.get(`${urls.URL_BACKEND}/users/${localStorage.getItem("idUser")}`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                if (res.data === null) {
                    setErrorIdUser(true);
                    return;
                }
                setUser(res.data);
                setTrimmedDataUrl(res.data.signature);
                sigPad.current.fromDataURL(res.data.signature);
            })
            .catch(error => console.log(error))

        axios.get(`${urls.URL_BACKEND}/user_has_question_response/${localStorage.getItem("idUser")}/consultation`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                setResponses(res.data);
                if (res.data !== undefined) {
                    checkEntryForVerification(res.data);
                }
            })
            .catch(error => console.log(error))
    }, []);

    function checkEntryForVerification(questionResponsesTab) {
        if (questionResponsesTab === undefined || questionResponsesTab === null) return;
        let newVerif = questionResponsesVerification;

        if (questionResponsesTab[27] === undefined || questionResponsesTab[27] === null || questionResponsesTab[27].responses === undefined || questionResponsesTab[27].responses === null) return;

        if (questionResponsesTab[27].responses.length !== 0) {
            newVerif.iHaveConcernsAbout = questionResponsesTab[27].responses.length > 2 ? false : true;
        }

        if (questionResponsesTab[30] === undefined || questionResponsesTab[30] === null || questionResponsesTab[30].responses === undefined || questionResponsesTab[30].responses === null) return;

        if (questionResponsesTab[30].responses.length !== 0) {
            newVerif.questionService = questionResponsesTab[30].responses.length > 2 ? false : true;
        }

        setQuestionResponseVerification(cloneDeep(newVerif));
    }

    function valid(itemValue, type, idQuestion) {
        setResponses(cloneDeep(updateTab(itemValue, type, idQuestion, questionResponses)));
        checkEntryForVerification(questionResponses);
    };

    //Contents
    let contentform, putSignature, showSignature, contents;

    putSignature = (
        <FormGroup tag="fieldset" className="form-group">
            <div className="container">
                <div className="row">
                    <div className="col-4 margin-left--12px">
                        <legend>Please sign here:</legend>
                    </div>
                    <div className="col-4 margin-button-clear">
                        <button onClick={clear} type="button" className="btn btn-outline-secondary btn-sm">Clear</button>
                    </div>
                </div>
            </div>
            <div className='border_grey  margin-top-10px'>
                <SignaturePad ref={(ref) => { sigPad = ref }} penColor='black' canvasProps={{ width: 690, height: 200 }} />
            </div>
        </FormGroup>
    );

    showSignature = (
        <FormGroup tag="fieldset" className="form-group">
            <div className="container">
                <div className="row">
                    <div className="col-4 margin-left--12px">
                        <legend>Signature:</legend>
                    </div>
                </div>
            </div>
            <img className="image_center signature" src={trimmedDataUrl} />
            <small className="form-text text-muted  margin-top-20px">Be assured, we guarantee our service for a period of two weeks from the date of your visit. If you feel that we did not achieve What
            was agreed upon at your consultation and/or we did not deliver to you our promise. By signing this form you take responsibility for the responses entered.</small>
            <small className="form-text text-muted  margin-top-20px">If you want to change your signature, click <a onClick={() => { updateSignature() }}>HERE</a></small>
        </FormGroup>
    );

    contentform = (
        <div>
            {redirectConsultation && <Redirect to={'/consultation'} />}
            {redirect && <Redirect to={`/user/${localStorage.getItem('idUser')}/consultation`} />}
            <Form>
                <legend className="titleConsultation">Lifestyle</legend>
                <img src={image15} className="image" alt="image15" />
                <FormGroup tag="fieldset" className="form-group">
                    <FormGroup>
                        <legend>I work in</legend>
                        <div class="container">
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Office environment" onChange={(item) => valid(item.target.value, item.target.type, 24)} type="checkbox" checked={checkResponse("Office environment", 24, questionResponses)}
                                    />{' '}
                                    Office environment
                                </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Outdoors" onChange={(item) => valid(item.target.value, item.target.type, 24)} type="checkbox" checked={checkResponse("Outdoors", 24, questionResponses)}
                                    />{' '}
                                    Outdoors
                                </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Indoors" onChange={(item) => valid(item.target.value, item.target.type, 24)} type="checkbox" checked={checkResponse("Indoors", 24, questionResponses)}
                                    />{' '}
                                    Indoors
                                </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Not applicable" onChange={(item) => valid(item.target.value, item.target.type, 24)} type="checkbox" checked={checkResponse("Not applicable", 24, questionResponses)}
                                    />{' '}
                                    Not applicable
                                </Label>
                            </FormGroup>
                        </div>
                    </FormGroup>
                    <FormGroup tag="fieldset" className="form-group">
                        <legend>I exercise</legend>
                        <div class="container">
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Indoors" onChange={(item) => valid(item.target.value, item.target.type, 25)} type="checkbox" checked={checkResponse("Indoors", 25, questionResponses)}
                                    />{' '}
                                    Indoors
                                </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Outdoors" onChange={(item) => valid(item.target.value, item.target.type, 25)} type="checkbox" checked={checkResponse("Outdoors", 25, questionResponses)}
                                    />{' '}
                                    Outdoors
                                </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Swim regularly" onChange={(item) => valid(item.target.value, item.target.type, 25)} type="checkbox" checked={checkResponse("Swim regularly", 25, questionResponses)}
                                    />{' '}
                                    Swim regularly
                                </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label check className="form-check-label">
                                    <Input className="form-check-input" value="Not applicable" onChange={(item) => valid(item.target.value, item.target.type, 25)} type="checkbox" checked={checkResponse("Not applicable", 25, questionResponses)}
                                    />{' '}
                                    Not applicable
                                </Label>
                            </FormGroup>
                        </div>
                    </FormGroup>
                    <FormGroup tag="fieldset" className="form-group">
                        <div className="hl-2" />
                        <Label className="titleConsultation">Personality</Label>
                        <div class="container">
                            <legend className="margin-left--10px">Choose a lot in which you identify yourself :</legend>
                            <FormGroup check className="form-check">
                                <Label className="">
                                    <Input type="radio" className="form-check-input " onChange={(item) => valid(item.target.value, item.target.type, 26)} name="radio26" value="Conservatrice/Classic" checked={checkResponse("Conservatrice/Classic", 26, questionResponses)} />
                                    Lot 1: [Conservatrice - Classic]
                                </Label>
                            </FormGroup>
                            <img src={lot1} className="image" alt="conservatrice" />
                            <FormGroup check className="form-check margin-right-20px ">
                                <Label className="form-check-label ">
                                    <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 26)} name="radio26" value="Dramatic/Eccentric/High fashion" checked={checkResponse("Dramatic/Eccentric/High fashion", 26, questionResponses)} />
                                    Lot 2: [Dramatic - Eccentric - High Fashion]
                                </Label>
                            </FormGroup>
                            <img src={lot2} className="image" alt="conservatrice" />
                            <FormGroup check className="form-check margin-right-20px">
                                <Label className="form-check-label ">
                                    <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 26)} name="radio26" value="Natural/Wash&Wear" checked={checkResponse("Natural/Wash&Wear", 26, questionResponses)} />
                                    Lot 3: [Natural -  Wash & Wear]
                                </Label>
                            </FormGroup>
                            <img src={lot3} className="image" alt="conservatrice" />
                        </div>
                    </FormGroup>
                    <FormGroup tag="fieldset" className="form-group">
                            <legend className="margin-left--10px">I see myself as :</legend>
                            <div className="container">
                                <div className="row">
                                    <div className="col-3">
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input"  onChange={(item) => valid(item.target.value, item.target.type, 27)} value="Natural" checked={checkResponse("Natural", 27, questionResponses)} />
                                                Natural
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input"  onChange={(item) => valid(item.target.value, item.target.type, 27)} value="Eccentric" checked={checkResponse("Eccentric", 27, questionResponses)} />
                                                Eccentric
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input"  onChange={(item) => valid(item.target.value, item.target.type, 27)} value="Wash & Wear" checked={checkResponse("Wash & Wear", 27, questionResponses)} />
                                                Wash & Wear
                                            </Label>
                                        </FormGroup>
                                    </div>
                                    <div className="col-3">
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 27)} value="Conservatrice" checked={checkResponse("Conservatrice", 27, questionResponses)} />
                                                Conservatrice
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 27)} value="Dramatic" checked={checkResponse("Dramatic", 27, questionResponses)} />
                                                Dramatic
                                            </Label>
                                        </FormGroup>
                                    </div>
                                    <div className="col-3">
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 27)} value="High Fashion" checked={checkResponse("High Fashion", 27, questionResponses)} />
                                                High Fashion
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check className="form-check">
                                            <Label className="form-check-label">
                                                <Input type="checkbox" className="form-check-input"  onChange={(item) => valid(item.target.value, item.target.type, 27)} value="Classic" checked={checkResponse("Classic", 27, questionResponses)} />
                                                Classic
                                            </Label>
                                        </FormGroup>
                                    </div>
                                </div>
                            </div>
                    </FormGroup>
                    <legend>Facial features</legend>
                    <legend>I have concerns about</legend>
                    <div class="container">
                        {questionResponsesVerification.iHaveConcernsAbout === false && <p class="text-success">Three points choosen is the maximal.</p>}
                        <div class="row">
                            <div class="col-3">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Nose")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Nose" checked={checkResponse("Nose", 28, questionResponses)} />
                                        Nose
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Chin")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Chin" checked={checkResponse("Chin", 28, questionResponses)} />
                                        Chin
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Lines")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Lines" checked={checkResponse("Lines", 28, questionResponses)} />
                                        Lines
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-3">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Neck")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Neck" checked={checkResponse("Neck", 28, questionResponses)} />
                                        Neck
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Cheeks")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Cheeks" checked={checkResponse("Cheeks", 28, questionResponses)} />
                                        Cheeks
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Crown")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Crown" checked={checkResponse("Crown", 28, questionResponses)} />
                                        Crown
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-3">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Eyes")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Eyes" checked={checkResponse("Eyes", 28, questionResponses)} />
                                        Eyes
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Forehead")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Forehead" checked={checkResponse("Forehead", 28, questionResponses)} />
                                        Forehead
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label class="form-check-label">
                                        <Input class="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Ears")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Ears" checked={checkResponse("Ears", 28, questionResponses)} />
                                        Ears
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-3">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "Body weight")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="Body weight" checked={checkResponse("Body weight", 28, questionResponses)} />
                                        Body weight
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" disabled={checkDisabled(questionResponses, questionResponsesVerification.iHaveConcernsAbout, 28, "None")} onChange={(item) => valid(item.target.value, item.target.type, 28)} type="checkbox" value="None" checked={checkResponse("None", 28, questionResponses)} />
                                        None
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                    <FormGroup>
                        <Input type="text" className="form-control" onChange={(item) => valid(item.target.value, item.target.type, 28)} placeholder="Other" value={otherResponse(28, questionResponses)} />
                    </FormGroup>
                </FormGroup>
                <div className="hl" />
                <Label className="titleConsultation">Goal</Label>
                <img src={image14} className="image" alt="image14" />
                <FormGroup tag="fieldset" className="form-group">
                    <legend>My main reason for coming today is</legend>
                    <div className="container">
                        <div className="row">
                            <div className="col-5">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="My image & style" checked={checkResponse("My image & style", 29, questionResponses)} />
                                        My image & style
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="I need a change" checked={checkResponse("I need a change", 29, questionResponses)} />
                                        I need a change
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="Relax" checked={checkResponse("Relax", 29, questionResponses)} />
                                        Relax
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="Connection to my hairstylist" checked={checkResponse("Connection to my hairstylist", 29, questionResponses)} />
                                        Connection to my hairstylist
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-4">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="Hair care & health" checked={checkResponse("Hair care & health", 29, questionResponses)} />
                                        Hair care & health
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="Special occasion" checked={checkResponse("Special occasion", 29, questionResponses)} />
                                        Special occasion
                                    </Label>
                                </FormGroup>
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 29)} type="checkbox" value="Maintenance" checked={checkResponse("Maintenance", 29, questionResponses)} />
                                        Maintenance
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <FormGroup>
                    <Label className="questionConsultation" for="exampleInputEmail1">My long-term goal is:</Label>
                    <Input type="textarea" className="form-control" onChange={(item) => valid(item.target.value, item.target.type, 30)} placeholder="Enter your long-term goal"
                        value={questionResponses && questionResponses.length && questionResponses[30 - 1].responses && questionResponses[30 - 1].responses.length > 0 ? questionResponses[30 - 1].responses[0]['response_text'] : null} />
                </FormGroup>
                <div className="box-question-most-value">
                    <legend className="label-inside-box margin-top-10px">What I value most on my journey through the salon is</legend>
                    {questionResponsesVerification.questionService === false && <p class="text-success label-inside-box">You can just select three points in this part.</p>}
                    <FormGroup tag="fieldset" className="form-group label-inside-box">
                        <div className="container">
                            <div className="row margin-bot-15px">
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Welcoming team")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Welcoming team" checked={checkResponse("Welcoming team", 31, questionResponses)} />
                                            Welcoming team
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Welcome Letter")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Welcome Letter" checked={checkResponse("Welcome Letter", 31, questionResponses)} />
                                            Welcome Letter
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "The quote")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="The quote" checked={checkResponse("The quote", 31, questionResponses)} />
                                            The quote
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Efficiency/Speed")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Efficiency/Speed" checked={checkResponse("Efficiency/Speed", 31, questionResponses)} />
                                            Efficiency/Speed
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Product samples")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Product samples" checked={checkResponse("Product samples", 31, questionResponses)} />
                                            Product samples
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Other")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Other" checked={checkResponse("Other", 31, questionResponses)} />
                                            Other
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Head massage")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Head massage" checked={checkResponse("Head massage", 31, questionResponses)} />
                                            Head massage
                                    </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Consistency")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Consistency" checked={checkResponse("Consistency", 31, questionResponses)} />
                                            Consistency
                                    </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Hygiene")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Hygiene" checked={checkResponse("Hygiene", 31, questionResponses)} />
                                            Hygiene
                                    </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Ambiance")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Ambiance" checked={checkResponse("Ambiance", 31, questionResponses)} />
                                            Ambiance
                                    </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Music")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Music" checked={checkResponse("Music", 31, questionResponses)} />
                                            Music
                                    </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Art Gallery/Decor")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Art Gallery/Decor" checked={checkResponse("Art Gallery/Decor", 31, questionResponses)} />
                                            Art Gallery/Decor
                                    </Label>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row margin-bot-15px">
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Appointment Reminder")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Appointment Reminder" checked={checkResponse("Appointment Reminder", 31, questionResponses)} />
                                            Appointment Reminder
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "My voice counts/be listened to")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="My voice counts/be listened to" checked={checkResponse("My voice counts/be listened to", 31, questionResponses)} />
                                            My voice counts/be listened to
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Recommendation/Advice for style")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Recommendation/Advice for style" checked={checkResponse("Recommendation/Advice for style", 31, questionResponses)} />
                                            Recommendation/Advice for style
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Recommendation/Advice for colour")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Recommendation/Advice for colour" checked={checkResponse("Recommendation/Advice for colour", 31, questionResponses)} />
                                            Recommendation/Advice for colour
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Recommendation/Advice for products")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Recommendation/Advice for products" checked={checkResponse("Recommendation/Advice for products", 31, questionResponses)} />
                                            Recommendation/Advice for products
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "What is new/special offers")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="What is new/special offers" checked={checkResponse("What is new/special offers", 31, questionResponses)} />
                                            What is new/special offers
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Connection with my stylist")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Connection with my stylist" checked={checkResponse("Connection with my stylist", 31, questionResponses)} />
                                            Connection with my stylist
                                        </Label>
                                    </FormGroup>

                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Refreshments, magazines")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Refreshments, magazines" checked={checkResponse("Refreshments, magazines", 31, questionResponses)} />
                                            Refreshments, magazines
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Focus on your well-being")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Focus on your well-being" checked={checkResponse("MoFocus on your well-beingrning", 31, questionResponses)} />
                                            Focus on your well-being
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Re-booking service")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Re-booking service" checked={checkResponse("Re-booking service", 31, questionResponses)} />
                                            Re-booking service
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Feedback follow-up")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Feedback follow-up" checked={checkResponse("Feedback follow-up", 31, questionResponses)} />
                                            Feedback follow-up
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check className="form-check">
                                        <Label className="form-check-label">
                                            <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Receiving our newsletter")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Receiving our newsletter" checked={checkResponse("Receiving our newsletter", 31, questionResponses)} />
                                            Receiving our newsletter
                                        </Label>
                                    </FormGroup>
                                </div>
                            </div>
                            <FormGroup check className="form-check">
                                <Label className="form-check-label">
                                    <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Time spent discovering and understanding your hair needs")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Time spent discovering and understanding your hair needs" checked={checkResponse("Time spent discovering and understanding your hair needs", 31, questionResponses)} />
                                    Time spent discovering and understanding your hair needs
                                    </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label className="form-check-label">
                                    <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Feeling valued through our VIP Program")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Feeling valued through our VIP Program" checked={checkResponse("Feeling valued through our VIP Program", 31, questionResponses)} />
                                    Feeling valued through our VIP Program
                                    </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label className="form-check-label">
                                    <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "That we are a team of leaders constantly evolving")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="That we are a team of leaders constantly evolving" checked={checkResponse("That we are a team of leaders constantly evolving", 31, questionResponses)} />
                                    That we are a team of leaders constantly evolving
                                    </Label>
                            </FormGroup>
                            <FormGroup check className="form-check">
                                <Label className="form-check-label">
                                    <Input type="checkbox" className="form-check-input" disabled={checkDisabledService(questionResponses, questionResponsesVerification.questionService, "Connection through social media")} onChange={(item) => valid(item.target.value, item.target.type, 31)} name="radio30" value="Connection through social media" checked={checkResponse("Connection through social media", 31, questionResponses)} />
                                    Connection through social media
                                    </Label>
                            </FormGroup>
                        </div>
                    </FormGroup>
                </div>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>Choose a shape</legend>
                    <img src={shape} className="image" alt="shape" />
                    <div className="container">
                        <div className="row">
                            <div className="col-3">
                                <FormGroup check className="form-check margin-right-20px">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input " onChange={(item) => valid(item.target.value, item.target.type, 32)} name="radio31" value="wave" checked={checkResponse("wave", 32, questionResponses)} />
                                        Wave
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-3">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 32)} name="radio31" value="Rectangle" checked={checkResponse("Rectangle", 32, questionResponses)} />
                                        rectangle
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 32)} name="radio31" value="Circle" checked={checkResponse("Circle", 32, questionResponses)} />
                                        circle
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 32)} name="radio31" value="Triangle" checked={checkResponse("Triangle", 32, questionResponses)} />
                                        triangle
                                    </Label>
                                </FormGroup>
                            </div>
                            <div className="col-2">
                                <FormGroup check className="form-check">
                                    <Label className="form-check-label">
                                        <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 32)} name="radio31" value="Square" checked={checkResponse("Square", 32, questionResponses)} />
                                        Square
                                    </Label>
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>Would you like to know about a VIP program ? (to save money)</legend>
                    <div className="container">
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 33)} name="radio32" value="Yes" checked={checkResponse("Yes", 33, questionResponses)} />
                                Yes
                            </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 33)} name="radio32" value="No" checked={checkResponse("No", 33, questionResponses)} />
                                No
                            </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>Please keep me informed via email about promotions, offers events relating to Marie-France Group</legend>
                    <div className="container">
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 34)} name="radio33" value="Yes" checked={checkResponse("Yes", 34, questionResponses)} />
                                Yes
                                    </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 34)} name="radio33" value="No" checked={checkResponse("No", 34, questionResponses)} />
                                No
                                    </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <FormGroup tag="fieldset" className="form-group">
                    <legend>I would like a quote:</legend>
                    <div className="container">
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 35)} name="radio37" value="Yes" checked={checkResponse("Yes", 35, questionResponses)} />
                                Yes
                                    </Label>
                        </FormGroup>
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input type="radio" className="form-check-input" onChange={(item) => valid(item.target.value, item.target.type, 35)} name="radio37" value="No" checked={checkResponse("No", 35, questionResponses)} />
                                No
                                    </Label>
                        </FormGroup>
                    </div>
                </FormGroup>
                <img src={image10} className="image" alt="image10" />
                <div className="hl" />
                {!trimmedDataUrl ? putSignature : null}
                {trimmedDataUrl ? showSignature : null}
            </Form>
            <Link to="/consultation/3"><Button onClick={() => { submitQuestionResponses(questionResponses, 24, 35); trim() }} className="btn btn-primary">Previous</Button></Link>
            <Button onClick={finishFunction} className="btn btn-primary">Finish</Button>
        </div>
    );

    async function finishFunction() {
        trim();
        await submitQuestionResponses(questionResponses, 24, 35);
        setRedirect(true);
        localStorage.removeItem('idUser');
    }

    contents = [<Message_UserIdentified user={user} callBack={setRedirectConsultation} />, contentform];
    if (login === false) contents = Message_NeedToBeConnected();
    if (errodIdUser === true) contents = Message_EmptyUserID();

    return (
        <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
            <div className="card-header">Lifestyle</div>
            <div className="card-body">
                {contents}
            </div>
        </div>
    );
}
export default Consultation4;
