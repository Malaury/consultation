import React from 'react';
import '../css-component/note10-system.scss';
import { Label } from 'reactstrap';
const SystemRateInput10 = (props) => {
    const valueCharged = props.valueCharged !== null ? props.valueCharged :'0';
    return (
        <div className="margin-left-15 margin-top-10px">
            <div className="row">
                <Label className="margin-left-31px">Bad - 1</Label>
                <Label className="margin-left-35px">2</Label>
                <Label className="margin-left-35px">3</Label>
                <Label className="margin-left-34px">4</Label>
                <Label className="margin-left-34px">5</Label>
                <Label className="margin-left-35px">6</Label>
                <Label className="margin-left-35px">7</Label>
                <Label className="margin-left-35px">8</Label>
                <Label className="margin-left-35px">9</Label>
                <Label className="margin-left-32px">10 - Good</Label>
            </div>
            <input class="medium input-system10" checked={valueCharged==='1'?"Checked":""} onChange={() => {props.callBack('1','radio',props.idQuestion)}} type="radio"  />
            <input class="medium input-system10" onChange={() => {props.callBack('2','radio',props.idQuestion)}} checked={valueCharged==='2'?"Checked":""} type="radio" />
            <input class="medium input-system10" onChange={() => {props.callBack('3','radio',props.idQuestion)}} checked={valueCharged==='3'?"Checked":""} type="radio" />
            <input class="medium input-system10" onChange={() => {props.callBack('4','radio',props.idQuestion)}} checked={valueCharged==='4'?"Checked":""} type="radio"  />
            <input class="medium input-system10" onChange={() => {props.callBack('5','radio',props.idQuestion)}} checked={valueCharged==='5'?"Checked":""} type="radio" />
            <input class="medium input-system10" onChange={() => {props.callBack('6','radio',props.idQuestion)}} checked={valueCharged==='6'?"Checked":""} type="radio" />
            <input class="medium input-system10" onChange={() => {props.callBack('7','radio',props.idQuestion)}} checked={valueCharged==='7'?"Checked":""} type="radio"  />
            <input class="medium input-system10" onChange={() => {props.callBack('8','radio',props.idQuestion)}} checked={valueCharged==='8'?"Checked":""} type="radio"  />
            <input class="medium input-system10" onChange={() => {props.callBack('9','radio',props.idQuestion)}} checked={valueCharged==='9'?"Checked":""} type="radio"  />
            <input class="medium input-system10" onChange={() => {props.callBack('10','radio',props.idQuestion)}} checked={valueCharged==='10'?"Checked":""} type="radio" />
        </div>
    );
}

export default SystemRateInput10;