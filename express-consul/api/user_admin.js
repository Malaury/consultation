//Imports
var bcrypt = require('bcrypt');
const express = require('express');
const router = express.Router();
const {validate} = require('../models/user_admin');
const jwtUtils = require('../utils/jwt.utils');
const db = require('../utils/mysqldb');
const mysql = require('mysql');

const log = require('loglevel');
log.enableAll();

router.post('/login', async (req, res) => {
    log.info("api/user_admin - post('/login') | Enter in function.");
    const { error } = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    let iduser_admin = await findUserId(req.body.email, req.body.password);  

    if(iduser_admin === 0 || iduser_admin === undefined) return res.status(404).send('ERROR 404 : Password and (or) email are false.');
    res.status(200).json({
        'token': jwtUtils.generateTokenForUser(iduser_admin)
    });
    log.info("api/user_admin - post('/login') | Send new token to user.");
    log.info("api/user_admin - post('/login') | Function finished.");
});

async function findUserId(email, password){
    log.info("api/user_admin - findUserId | Enter in function.");
    return new Promise( (resolve,reject) => {
        const queryString = `SELECT * FROM user_admin WHERE email = ? AND password = ?`;
        db.connection.query(queryString, [email,password], (err, rows) => {
            if(err) reject(err);
            else if(rows.length != 0){
                log.info("api/user_admin - findUserId | Get user_admin data by email and password ("+ email + ") : "+ rows+".");
                log.info("api/user_admin - findUserId | Function finished.");
                resolve(rows[0].iduser_admin);
            }
            log.error("api/user_admin - findUserId | Does not find the user in db.");
            log.info("api/user_admin - findUserId | Function finished.");
            resolve(0);
        });
    });
}

router.get('/refresh', (req, res) => {
    log.info("api/user_admin - get('/refresh') | Enter in function.");
    var token = req.headers.token;
    if(token === undefined || token === null) return res.status(400).send('Error 400 : Need a token to refresh it.');
    var newToken = jwtUtils.refreshTokenForUser(token);
    if(newToken === false) return res.status(403).send("Error 403 - Access refused.");
    res.status(200).json({
        'token': newToken
    });
    log.info("api/user_admin - get('/refresh') |  Sent new Token to user: " + newToken + ".");
    log.info("api/user_admin - get('/refresh') |  Function finished.");
});

module.exports = router;