const Joi = require('joi');

function validateResponse(response){
    const schema = {
        question_id: Joi.number().required(),
        response_id: Joi.number().required(),
        user_id: Joi.number().required()
    };
    return Joi.validate(response, schema);
}

// validateFuturResponse is one of the data send to /user_has_question_response/:id to add a response in db.
function validateFuturResponse(futurResponse){
    const schema = {
        question_id: Joi.number().required(),
        response_text: Joi.string().required(),
        type: Joi.string().required()
    };
    return Joi.validate(futurResponse, schema);
}

exports.validate = validateResponse;
exports.validateFuturResponse = validateFuturResponse;