const express = require('express');
const router = express.Router();
const {validate} = require('../models/user_has_question_response');
const {validateFuturResponse} = require('../models/user_has_question_response');
const jwtUtils = require('../utils/jwt.utils');
const db = require('../utils/mysqldb');
const mysql = require('mysql');

const log = require('loglevel');
log.enableAll();

router.post('/:id/:idMin/:idMax', async (req,res) => {
    log.info("api/user_has_question_response - post('/:id/:idMin/:idMax') | Enter in function.");
    const idUser = req.params.id;
    if( await findOneUser(idUser) === 0) return res.status(404).send("Error 404 - User not found.");

     //delete last questions to update
     const idMin = req.params.idMin;
     const idMax = req.params.idMax;
     if((idMin !== '1' && idMin !== '14' && idMin !== '24') || (idMax !== '13' && idMax !== '23' && idMax !== '35')) return res.status(404).send("Error 404 - idMax or idMin are wrong.");
     deleteResponses(idUser, req.params.formname, idMin, idMax);

    var questionResponseTab = req.body.questionResponseTab;
    if(questionResponseTab === undefined || questionResponseTab == null) return res.status(400).send("ERROR 400 - We need questionResponseTab not empty.");

    for(var cpt =0; cpt < questionResponseTab.length ; cpt ++){
        const { error } = validateFuturResponse (questionResponseTab[cpt]);
        if (error) return res.status(400).send(error.details[0].message);
        
        //On regarde si l'id de la question existe
        const idQuestion = questionResponseTab[cpt].question_id;
        if( await findOneQuestion(idQuestion) === 0) return res.status(404).send("Error 404 - Question id not found.");
        log.info("api/user_has_question_response - post('/:id/:idMin/:idMax') | Question was found");
        
        //On recherche l'id de la response sinon on l'a cree
        const textResponse = questionResponseTab[cpt].response_text;
        let idResponse = await findOneResponse(textResponse);
        if(idResponse === -1 ){
            await postOneResponse(textResponse);
            idResponse = await findOneResponse(textResponse);
        }
        await sendOneUserHasQuestionResponse(idUser, idQuestion, idResponse, questionResponseTab[cpt].type);
    }  
    res.send(req.body.questionResponseTab);
    log.info("api/user_has_question_response - post('/:id/:idMin/:idMax') | Function finished.");
});

async function sendOneUserHasQuestionResponse(idUser, idQuestion, idResponse, type){
    log.info("api/user_has_question_response - sendOneUserHasQuestionResponse | Enter in function.");
    return new Promise((resolve, reject) => {
        const user_has_question_response = ({
            question_id: idQuestion,
            response_id: idResponse,
            user_id: idUser,
            type : type
        });
        const queryString = `INSERT INTO user_has_question_response (\`question_id\`,\`response_id\`,\`user_id\`, \`type\`) VALUES (?,?,?,?)`
        db.connection.query(queryString, [user_has_question_response.question_id,user_has_question_response.response_id,user_has_question_response.user_id,user_has_question_response.type], (err, rows) => {
            if(err) reject(err);
            log.info("api/user_has_question_response - sendOneUserHasQuestionResponse | Insert user_has_question_response: "+ user_has_question_response + ".");
        });
        log.info("api/user_has_question_response - sendOneUserHasQuestionResponse | Function finished.");
        resolve(user_has_question_response);
    }) 
}

async function findOneUser(id){
    log.info("api/user_has_question_response - findOneUser | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `SELECT * FROM user WHERE iduser=?`;
        db.connection.query(queryString,[id], (err, rows) => {
            if(err) reject(err);
            log.info("api/user_has_question_response - findOneUser | User found: "+ rows.length +".");
            log.info("api/user_has_question_response - findOneUser | Function finished.");
            resolve(rows.length);
        });
    });  
}

async function postOneResponse(textResponse){
    log.info("api/user_has_question_response - postOneResponse | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `INSERT INTO response (\`text\`) VALUES (?)`
        db.connection.query(queryString, [textResponse], (err, rows) => {
            if(err) reject(err);
            log.info("api/user_has_question_response - postOneResponse | Insert response: "+textResponse+".");
            log.info("api/user_has_question_response - postOneResponse | Function finished.");
            resolve(textResponse);
        });
    });
}

async function findOneResponse(textResponse){
    log.info("api/user_has_question_response - findOneResponse | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `SELECT * FROM response WHERE text=?`
        db.connection.query(queryString,[textResponse], (err, rows) => {
            if(err) reject(err);
            if(rows.length === 0){
                log.error("api/user_has_question_response - findOneResponse | No response was found.");
                log.info("api/user_has_question_response - findOneResponse | Function finished.");
                resolve(-1);
            }
            else{
                log.info("api/user_has_question_response - findOneResponse | Function finished.");
                resolve(rows[0].idresponse);
            }
        });
    });
}

async function findOneQuestion(id){
    log.info("api/user_has_question_response - findOneQuestion | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `SELECT * FROM question WHERE idquestion =?`
        db.connection.query(queryString,[id], (err, rows) => {
            if(err) reject(err);
            log.info("api/user_has_question_response - findOneQuestion | Function finished.");
            resolve(rows.length);
        });
    });
}

async function deleteResponses(idUser, formname, questionIdMin, questionIdMax){
    log.info("api/user_has_question_response - deleteResponses | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `DELETE FROM user_has_question_response WHERE (\`question_id\`>= ?) and (\`question_id\` <= ?) and (\`user_id\` = ?)`;
        db.connection.query(queryString, [questionIdMin,questionIdMax,idUser], (err, rows) => {
            if(err) reject(err);
            log.info("api/user_has_question_response - deleteResponses | The response from the question: " + questionIdMin + "to: " + questionIdMax + "was deleted.");
            log.info("api/user_has_question_response - deleteResponses | Function finished.");
            resolve(rows.length);
        });
    });
}

router.get('/:iduser/:formname', async (req,res) => {
    log.info("api/user_has_question_response - get('/:iduser/:formname') | Enter in function.");
    if(jwtUtils.decodeTokenForUser(req.headers.token) === false) return res.status(403).send("Error 403 - Access refused.");
    const idUser = req.params.iduser;
    if( await findOneUser(idUser) === 0) return res.status(404).send("Error 404 - User not found.");

    const questions = await findQuestions(req.params.formname);
    if(questions === undefined || questions.length === 0) return res.status(404).send("Error 404 - Form not found");
    log.info("api/user_has_question_response - get('/:iduser/:formname') | Questions was found.");

    let questionResponses = [];
    for(cpt=0; cpt<questions.length; cpt++){
        const questionResponse = {
            question_id: 0,
            question_text: "",
            responses: []
        }
    
        questionResponse.question_id=questions[cpt].idquestion;
        questionResponse.question_text=questions[cpt].text;
        let responsesDTO = [];
        let responsesFound= await searchResponsesByQuestionAndUserId(questions[cpt].idquestion, idUser);
        for(let responseDB of responsesFound){
            if(responseDB !== undefined){
                const responseDTO = {
                    response_id:0,
                    response_text:"",
                    response_type:""
                }
                const responseEntity = await searchResponseByResponseId(responseDB.response_id);
                responseDTO.response_id = responseDB.response_id;
                responseDTO.response_text= responseEntity.text;
                responseDTO.response_type= responseDB.type;
                responsesDTO.push(responseDTO);
            }
        }
        questionResponse.responses=responsesDTO;
        questionResponses[cpt]=questionResponse;
    }
    res.send(questionResponses);
    log.info("api/user_has_question_response - get('/:iduser/:formname') |Function finished.");
});

async function searchResponseByResponseId(idResponse){
    log.info("api/user_has_question_response - searchResponseByResponseId | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `SELECT * FROM response WHERE idresponse=?`
        db.connection.query(queryString,[idResponse], (err, rows) => {
            if(err) reject(err);
            log.info("api/user_has_question_response - searchResponseByResponseId | Response found: "+ rows[0] +".");
            log.info("api/user_has_question_response - searchResponseByResponseId | Function finished.");
            resolve(rows[0]);
        });
    });
}

async function searchResponsesByQuestionAndUserId(question_id, user_id){
    log.info("api/user_has_question_response - searchResponsesByQuestionAndUserId | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `SELECT * FROM user_has_question_response WHERE user_id=? AND question_id=?`
        db.connection.query(queryString, [user_id,question_id], (err, rows) => {
        if(err) reject(err);
        log.info("api/user_has_question_response - searchResponsesByQuestionAndUserId | Responses Found By question and user Id: "+rows+".");
        log.info("api/user_has_question_response - searchResponsesByQuestionAndUserId | Function finished.");
        resolve(rows);
        });
    });
}

async function findQuestions(formName){
    log.info("api/user_has_question_response - findQuestions | Enter in function.");
    return new Promise( (resolve, reject) => {
        const queryString = `SELECT * FROM question WHERE form=?`
        db.connection.query(queryString, [formName], (err, rows) => {
        if(err) reject(err);
        log.info("api/user_has_question_response - findQuestions | Function finished.");
        resolve(rows);
        });
    });
}
module.exports = router;