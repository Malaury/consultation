var mailer = require("nodemailer");

var smtpTransport = mailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        user: "mariefrancegroupforms@gmail.com",
        pass: "hY469kUdV"
    }}
);

let from = `Consultation <mariefrancegroupforms@gmail.com>`;
module.exports = {
    sendNewUserEmail: function (user) {
        var mail = {
            from: from,
            //to: "info@mariefrancegroup.com.au",
            to: "malaumalau60@gmail.com",
            subject: `Marie-France consultation: New client ${user.firstname} ${user.lastname}`,
            html: `<h1>New client for kitomba</h1>
            <ul>
                <li>Firstname: ${user.firstname}</li>
                <li>Lastname: ${user.lastname}</li>
                <li>Phone: ${user.phone}</li>
                <li>Mail: ${user.mail}</li>
                <li>Address: ${user.address}</li>
                <li>Suburb: ${user.suburb}</li>
                <li>State: ${user.state}</li>
                <li>Postal code: ${user.postalcode}</li>
            </ul>`
        }

        smtpTransport.sendMail(mail, function (error, response) {
            if (error) {
                console.log("Erreur lors de l'envoie du mail!");
                console.log(error);
            } else {
                console.log("Mail envoyé avec succès!")
            }
            smtpTransport.close();
        });
    },


    sendUpdateUserEmail: function (user, difference) {
        if(difference===null || difference===[]|| difference=== undefined) return;

        let updateMailContent = '';
        if(difference.find(r => r ==="firstname") !== undefined) updateMailContent+=`<li>Firstname: ${user.firstname}</li>`
        if(difference.find(r => r ==="lastname") !== undefined) updateMailContent+=`<li>Lastname: ${user.lastname}</li>`
        if(difference.find(r => r ==="phone") !== undefined) updateMailContent+=`<li>Phone: ${user.phone}</li>`
        if(difference.find(r => r ==="mail") !== undefined) updateMailContent+=`<li>Mail: ${user.mail}</li>`
        if(difference.find(r => r ==="address")!== undefined) updateMailContent+=`<li>Address: ${user.address}</li>`
        if(difference.find(r => r ==="suburb") !== undefined) updateMailContent+=`<li>Suburb: ${user.suburb}</li>`
        if(difference.find(r => r ==="state") !== undefined) updateMailContent+=`<li>State: ${user.state}</li>`
        if(difference.find(r => r ==="postalcode") !== undefined) updateMailContent+=`<li>Postal code: ${user.postalcode}</li>`
        if(updateMailContent === '') return;
        
        var mail = {
            from: from,
            //to: "info@mariefrancegroup.com.au",
            to: "malaumalau60@gmail.com",
            subject: `Client update: ${user.firstname} ${user.lastname}`,
            html: `<h1>Client updated</h1>
            <h3>Here are the parts to be updated: </h3>
            <ul>
                ${updateMailContent}
            </ul>
            -----------------
            <h4> Status of client data before change </h4>
            <ul>
                <li>Firstname: ${user.firstname}</li>
                <li>Lastname: ${user.lastname}</li>
                <li>Phone: ${user.phone}</li>
                <li>Mail: ${user.mail}</li>
                <li>Address: ${user.address}</li>
                <li>Suburb: ${user.suburb}</li>
                <li>State: ${user.state}</li>
                <li>Postal code: ${user.postalcode}</li>
            </ul>`
        }

        smtpTransport.sendMail(mail, function (error, response) {
            if (error) {
                console.log("Erreur lors de l'envoie du mail!");
                console.log(error);
            } else {
                console.log("Mail envoyé avec succès!")
            }
            smtpTransport.close();
        });
    }
}
