import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootswatch/dist/lux/bootstrap.css';
import './index.css';
import './component/header.js';
import Header from './component/header.js'

class Structure extends React.Component{
    render() {
      return (
        <div className="structure">
          <Header/>
        </div>
      );
    }
  }
  
  ReactDOM.render(
    <Structure/>,
    document.getElementById('root')
    );
