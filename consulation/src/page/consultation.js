import React, { useState, useEffect } from 'react';
import { Label, FormGroup, Form, Input} from 'reactstrap';
import axios from 'axios';
import { Message_NeedToBeConnected, Message_UserIdentified } from '../component/message_consultation';
import image1 from '../img/consultation/1.png';
import image2 from '../img/consultation/2.jpg';
import {Redirect} from 'react-router-dom';
const jwtUtils = require('../utils/jwtUtils');
const urls = require('../utils/frontUtils');

const Consultation = () => {
    const [user, setUser] = useState(null);
    const [login] = useState(jwtUtils.checkToken());
    const [userId, setUserId] = useState(false);
    const [userVerification, setUserVerification] = useState({
        firstnameValid: false,
        firstanemTouched: false,
        lastnameValid: false,
        lastnameTouched: false,
        phoneValid: false,
        phoneTouched: false,
        phoneAlreadyUsed: false,
        phoneCharged: false,
    });
    const [firstnameClassName, setFirstnameClassName] = useState("form-control");
    const [lastnameClassName, setLastnameClassName] = useState("form-control");
    const[redirect,setRedirect] = useState(false);
    console.log("user", userId);

    let photoClassName = "";
    if (userVerification.phoneTouched === false) {
        photoClassName = 'form-control';
    }
    else if ((userVerification.phoneValid === true && userVerification.phoneAlreadyUsed === false) || userVerification.phoneCharged === true) {
        photoClassName = 'form-control is-valid';
    }
    else {
        photoClassName = 'form-control is-invalid';
    }

    function handleChange(event) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });

        checkEntryForVerification(event.target.value, event.target.name);
    }

    function checkEntryForVerification(value, name) {
        let updateUserVerification = userVerification;
        if (name === 'firstname') {
            updateUserVerification.firstanemTouched = true;
            updateUserVerification.firstnameValid = (value !== "") ? true : false;
            if (updateUserVerification.firstnameValid === true) {
                 setFirstnameClassName('form-control is-valid');
            }
            else{
                setFirstnameClassName('form-control is-invalid');
            }
        }
        if (name === 'lastname') {
            updateUserVerification.lastnameTouched = true;
            updateUserVerification.lastnameValid = value !== "" ? true : false;
            if (updateUserVerification.lastnameValid === true) {
                setLastnameClassName('form-control is-valid');
           }
           else{
               setLastnameClassName('form-control is-invalid');
           }
        }
        setUserVerification(updateUserVerification);

        if (name === 'phone') {
            updateUserVerification.phoneTouched = true;
            updateUserVerification.phoneValid = (value === "" ) ? false :true;
            //updateUserVerification.phoneValid = /[0-9]{10}/.test(value);
            //if (updateUserVerification.phoneAlreadyUsed === true && updateUserVerification.phoneValid === false) updateUserVerification.phoneAlreadyUsed = false; 
            updateUserVerification.phoneAlreadyUsed = false;
            if (updateUserVerification.phoneCharged === false && updateUserVerification.phoneValid === true) {
                axios.get(`${urls.URL_BACKEND}/users/phone/${value}`, {
                    headers: {
                        'token': localStorage.getItem('token')
                    }
                }).then(res => {
                    if (res.data.length > 0) updateUserVerification.phoneAlreadyUsed = true;
                    setUserVerification(Object.assign({}, updateUserVerification));
                })
            } else {
                setUserVerification(Object.assign({}, updateUserVerification));
            }
            console.log(updateUserVerification)
        }
    };

    function verificationBeforeSubmit() {
        return (userVerification.firstnameValid && userVerification.lastnameValid &&
            userVerification.phoneValid ) ? true : false;
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        if (login === false || localStorage.getItem("idUser") === null) return;
        axios.get(`${urls.URL_BACKEND}/users/${localStorage.getItem("idUser")}`, {
            headers: {
                'token': localStorage.getItem('token')
            }
        })
            .then(res => {
                setUser(res.data);
                if (res.data !== undefined) {
                    checkEntryForVerification(res.data.phone, 'phone');
                    checkEntryForVerification(res.data.phone, 'firstname');
                    checkEntryForVerification(res.data.phone, 'lastname');
                    let newVerification = userVerification;
                    userVerification.phoneCharged = true;
                    setUserVerification(newVerification);
                    setUserId(true);
                }
            })
            .catch(error => {
                if(error.status === 403) localStorage.removeItem('token')})
    }, []);

    function submit() {
        jwtUtils.checkToken();
        if (localStorage.getItem("idUser") !== null) {
            axios.put(`${urls.URL_BACKEND}/users/${localStorage.getItem("idUser")}`, user, {
                headers: {
                    'token': localStorage.getItem('token')
                }
            }).then( res => {
                setRedirect(true)
            });
        }
        else {
            console.log("here");
            axios.post(`${urls.URL_BACKEND}/users`, user, {
                headers: {
                    'token': localStorage.getItem('token')
                }
            }).then(res => {
                localStorage.setItem('idUser', res.data.iduser);
                setRedirect(true);
            });
        }
    }

    function restart(){
        setUser(null); 
        setUserId(false); 
        setUserVerification({
            firstnameValid: false,
            firstanemTouched: false,
            lastnameValid: false,
            lastnameTouched: false,
            phoneValid: false,
            phoneTouched: false,
            phoneAlreadyUsed: false,
            phoneCharged: false
        });
        setLastnameClassName('form-control');
        setFirstnameClassName('form-control');
    }

    let contents, contentform;
    window.onbeforeunload = function(){ if(verificationBeforeSubmit()) submit()};

    contentform = (
        <div >
            {redirect === true && <Redirect to='/consultation/2'/>}
            <Form>
                <FormGroup>
                    <img src={image1} className="image" alt="image1" />
                    <legend className="titleConsultation">Together we create</legend>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation" htmlFor="exampleInputEmail1">Email address</Label>
                        <Input type="email" autoComplete="off" name="mail" className="form-control" value={user ? user.mail : ""} aria-describedby="emailHelp" placeholder={"Enter email"} onChange={handleChange} />
                        <small className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </FormGroup>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">First name *</Label>
                        <Input type="text" autoComplete="off" name="firstname" className={firstnameClassName} value={user ? user.firstname : ""} placeholder={"Enter your first name"} onChange={handleChange} />
                    </FormGroup>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">Last name *</Label>
                        <Input type="text" autoComplete="off" name="lastname" className={lastnameClassName} value={user ? user.lastname : ""} placeholder={"Enter your last name"} onChange={handleChange} />
                    </FormGroup>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">Phone *</Label>
                        <Input type="text" autoComplete="off" name="phone" className={photoClassName} value={user ? user.phone : ""} placeholder={"Enter your phone number"} onChange={handleChange} />
                        {userVerification.phoneAlreadyUsed === true && <div class="invalid-feedback">Sorry, this phone number's taken. Try another?</div>}
                    </FormGroup>
                    <FormGroup className="form-group">
                        <legend>Best contact </legend>
                        <FormGroup check className="form-check">
                            <Label className="form-check-label">
                                <Input name="bestcontact" type="radio" className="form-check-input" value="Phone"
                                    checked={(user) && (user.bestcontact === "Phone") ? "Phone" : ""} onChange={handleChange} />
                                Phone
                                </Label>
                        </FormGroup>
                        <FormGroup className="form-check">
                            <Label className="form-check-label">
                                <Input name="bestcontact" type="radio" className="form-check-input" value="Email"
                                    checked={(user) && (user.bestcontact === "Email") ? "Email" : ""} onChange={handleChange} />
                                Email
                                </Label>
                        </FormGroup>
                    </FormGroup>
                    <legend className="titleConsultation">Localisation</legend>
                    <img src={image2} className="image" alt="image2" />
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">Address</Label>
                        <Input type="text" autoComplete="off" className="form-control" name="address" value={user ? user.address : ""} placeholder={"Enter your address"} onChange={handleChange} />
                    </FormGroup>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">Suburb</Label>
                        <Input type="text" autoComplete="off" className="form-control" name="suburb" value={user ? user.suburb : ""} placeholder={"Enter your suburb"} onChange={handleChange} />
                    </FormGroup>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">State</Label>
                        <Input type="select" name="state" className="form-control" onChange={handleChange} value={user ? user.state : ""}>
                            <option>NSW</option>
                            <option>VIC</option>
                            <option>QLD</option>
                            <option>TAS</option>
                            <option>SA</option>
                            <option>WA</option>
                        </Input>
                    </FormGroup>
                    <FormGroup className="form-group">
                        <Label className="questionConsultation">Post code</Label>
                        <Input type="text" autoComplete="off" className="form-control" name="postalcode" value={user ? user.postalcode : ""} placeholder={"Enter your postal code"} onChange={handleChange} />
                    </FormGroup>
                    <Label className="questionConsultation">Birthday</Label>
                    <Input type="date" value={user && user.birthday ? user.birthday.substr(0, 10) : ""} placeholder={"Enter your birthday"} name="birthday" onChange={handleChange} />
                </FormGroup>
            </Form>{!verificationBeforeSubmit() && (userVerification.firstanemTouched === true || userVerification.lastnameTouched === true|| userVerification.phoneTouched === true) && <p class="text-danger">Be careful to have fill all the obligatory inputs (firstname, lastname and phone).</p>}
            <div className="container">
                <div className="row">
                    {verificationBeforeSubmit() && <button onClick={submit} type="button" className="btn btn-primary">Next</button>}
                    {!verificationBeforeSubmit() && <button type="button" className="btn btn-primary disabled">Next</button>}
                    
                </div>
            </div>
        </div>
    );

    if (login === false || jwtUtils.checkToken() === false) contents = Message_NeedToBeConnected();
    else if (userId === false) contents = contentform;
    else if (user) { contents = [<Message_UserIdentified user={user} callBack={restart}/>, contentform]; }
    return (
        <div className="case card border-secondary mb-3" styles="max-width: 20rem;">
            <div className="card-header">Consultation</div>
            <div className="card-body">
                {contents}
            </div>
        </div>
    );

}
export default Consultation;